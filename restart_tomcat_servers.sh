#!/bin/bash


# Declare array for domains
declare -a ARRAY

# Populate array from domains listed in /var/www/servercontrol/conf/bootlist.conf
while read line ; do
    ARRAY+=($line)
done < /var/www/servercontrol/conf/bootlist.conf

# ask user whether they want to select the domains to restart
echo "Restart all Tomcat servers on this host? (Default: No -- select individual servers) [Y/N]:  "
read INT

# set ALL variable based on response
if [ "$INT" == "Y" ] || [ "$INT" == "y" ]
then

echo "Are you sure you want to restart all of the following Tomcat servers?"

#set up loop through array of domains
COUNTER=0
while [  $COUNTER -lt ${#ARRAY[@]} ]; do

echo -e "${ARRAY[$COUNTER]}"
let COUNTER=COUNTER+1

done

echo -e "\n(Default: No -- select individual servers) [Y/N]:  "
read IN

if [ "$IN" == "Y" ] || [ "$IN" == "y" ]
then
ALL=1 

else 
ALL=0 

fi

else
ALL=0

fi

#set up loop through array of domains
COUNTER=0
while [  $COUNTER -lt ${#ARRAY[@]} ]; do

# if ALL variable set to 0, ask about current domain in loop
if [ "$ALL" == "0" ]
then
echo "Restart ${ARRAY[$COUNTER]}? (default: No) [Y/N]:  " 
read ASK

# if response is not yes, move on to next domain
if [ "$ASK" != "Y" ] && [ "$ASK" != "y" ]
then 
echo -e "Skipping...\n"
let COUNTER=COUNTER+1
continue 

# end both if statements
fi

fi

# otherwise stop and then start Tomcat for domain and move to next
/var/www/servercontrol/cgi-bin/servercontrol-api -domain ${ARRAY[$COUNTER]} stop
echo -e "Stopped ${ARRAY[$COUNTER]}\n"

/var/www/servercontrol/cgi-bin/servercontrol-api -domain ${ARRAY[$COUNTER]} start
echo -e "Started ${ARRAY[$COUNTER]}\n"

let COUNTER=COUNTER+1

#end loop
done

echo -e "List of Tomcat servers complete\n"
