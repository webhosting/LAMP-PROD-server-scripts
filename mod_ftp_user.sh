#!/bin/bash

# Get the directory the script resides in.
SCRIPTPATH=$( cd $(dirname $0) ; pwd -P )

${SCRIPTPATH}/del_ftp_user.sh
${SCRIPTPATH}/add_ftp_user.sh
