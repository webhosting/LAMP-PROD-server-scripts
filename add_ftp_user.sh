#!/bin/bash

PRODUCT_ROOT_D=`grep ^PRODUCT_ROOT_D /etc/psa/psa.conf | awk '{print $2}'`
HTTPD_VHOSTS_D=`grep ^HTTPD_VHOSTS_D /etc/psa/psa.conf | awk '{print $2}'`
CHROOT_ROOT_D="$HTTPD_VHOSTS_D/chroot"

# For the chroot SSH/SFTP to work, the shell must be the the chroot shell.
usermod -s /usr/local/psa/bin/chrootsh ${NEW_SYSTEM_USER}

# Configure the chroot environment.
${PRODUCT_ROOT_D}/admin/sbin/chrootmng --create --source=${CHROOT_ROOT_D} --target=${NEW_HOME_DIRECTORY}

# Next, add the user to the chroot passwd file.  You need to get the user's
# UID and GID.
ID_INFO=$(id ${NEW_SYSTEM_USER})
NEW_UID=$(echo ${ID_INFO} | sed -e 's/^.*uid=\([0-9]*\).*$/\1/')
NEW_GID=$(echo ${ID_INFO} | sed -e 's/^.*gid=\([0-9]*\).*$/\1/')
NEW_HOME_SUBDIRECTORY="/"

echo "${NEW_SYSTEM_USER}:x:${NEW_UID}:${NEW_GID}::${NEW_HOME_SUBDIRECTORY}:/bin/bash" >> ${NEW_HOME_DIRECTORY}/etc/passwd
