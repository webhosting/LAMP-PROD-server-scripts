#!/bin/bash

###############################################################################
###############################################################################
# This script provides the following:
#	1. A list of all DBs on the host that are formatted for WP or Drupal
#	2. The domain name each DB belongs to and the email to contact the customer
#	3. The version of WP or Drupal (where possible)
#	4. The ability to send an email to all WP or Drupal customers via command line**
#	5. A list of all the emails for the WP customers and Drupal customers
#
# **When run without command line arguments, no emails are sent.
#   To send emails:
#	1. Edit the content of the email in the appropriate section below
#	2. Provide one of the following arguments: wp_sendmail drupal_sendmail
#	   You cannot provide both at once.  To email both sets of customers,
#	   you have to run the script twice.
#
#  -Ben Sousa 12/1/2014
###############################################################################
###############################################################################

# HOST-SPECIFIC VALUES (CONTAINED IN SEPARATE FILE)
. /usr/local/psa/bin/scripts/host_values

#read command line argument to see if emails should be sent
send_mail=$1

###LOGGING######################################
echo ""
echo "Errors are logged to $man_logfile"

# man_logfile gets a lot of junk, because there are always permissions and not found errors
# as the find and sed commands are executed

#generate header in man_logfile
echo "========================================================================" >> $man_logfile 2>> $man_logfile

# information on the event date and time
/bin/date >> $man_logfile 2>> $man_logfile

# information on the user, on behalf of which the script was executed (to ensure control)
/usr/bin/id >> $man_logfile 2>> $man_logfile

echo "========================================================================" >> $man_logfile 2>> $man_logfile

##############################################################################################
#Print Date to screen
/bin/date

# list of all databases
all_dbs="$(mysql -u $user -p$pass -Bse 'show databases')"

##############################################################################################
##############################################################################################
#      WORDPRESS
##############################################################################################
##############################################################################################

echo ""
echo "========================"
echo "Wordpress DBs on $server"
echo "========================"
echo ""
printf "%-35s\t %-50s\t %-45s\t %-7s\t %-9s\t %-11s\n" "DB NAME" "DOMAIN" "CONTACT" "VERSION" "MULTIPLE*" "EMAIL SENT?"

wp_emails=()

for db in $all_dbs
do

all_tables="$(mysql -u$user -p$pass $db -sN -e "SHOW TABLES LIKE '%_usermeta'" 2>> $man_logfile)"

for table in $all_tables
do

table_prefix="$(echo $table | sed -n "s/^\(.*\)_usermeta$/\1/p")"
            wp_db="$(mysql -u$user -p$pass $db -sN -e "SELECT meta_key FROM $table WHERE meta_key LIKE '${table_prefix}_%' LIMIT 1" 2>> $man_logfile)"
                if [ ! -z "$wp_db" ]
         	then
	            dbquery="$(mysql -u$user -p$pass psa -sN -e "SELECT dm.displayName, cl.email FROM
			domains AS dm, clients AS cl, data_bases AS db
			WHERE db.name='$db' AND db.dom_id=dm.id AND dm.cl_id=cl.id")"

array=( $( for i in $dbquery ; do echo $i ; done ) )

## DETERMINE PATH ####################

#IF PATH RESOLVES
if path=$(find /var/www/vhosts/${array[0]}/httpdocs -path "*/wp-config.php" 2>>$man_logfile -exec grep -l "$db" '{}' \; 2>>$man_logfile | sed 's/\/wp-config\.php//'  2>>$man_logfile ); then

#SET multi variable to default of N
multi="N"

#CHECK IF PATH RESOLVES TO MULTIPLE LOCATIONS
if [[ "$path" =~ ".*httpdocs.*var.*" ]]; then
#SET THE MULTI FLAG
multi="Y"

#IF MULTIPLE, SEE IF ONE OF THEM ENDS IN HTTPDOCS
if [[ "$path" =~ ".*httpdocs.{1,2}\/var.*" ]] ||
[[ "$path" =~ "^.*httpdocs$" ]]; then

#IF SO, USE THAT PATH TO DETERMINE VERSION
path="/var/www/vhosts/${array[0]}/httpdocs"

#OTHERWISE, USE THE FIRST PATH IN THE GROUP
else
path=$(echo $path | sed -n 's/^\(\/var\S*\)\s.*$/\1/p' 2>>$man_logfile )
fi

#IF NOT MULTI, USE PATH TO DETERMINE VERSION
else
path=$path
fi

#IF PATH DID NOT RESOLVE VERSION WILL NOT RESOLVE AND BE UNKNOWN

fi

## DETERMINE VERSION ####################

#IF VERSION RESOLVES BASED ON PATH, USE IT
if version=$(sed -n "s/.*wp_version = '\(.*\)'.*/\1/p" $path/wp-includes/version.php 2>>$man_logfile ); then

version=$(sed -n "s/.*wp_version = '\(.*\)'.*/\1/p" $path/wp-includes/version.php 2>>$man_logfile )

#OTHERWISE, QUESTION MARK
else
version="?"
fi

#build emails list that only includes each customer email one time
sent="N"
curr_email=${array[1]}
exists=0

for i in "${!wp_emails[@]}"; do
if [ "${wp_emails[$i]}" == "$curr_email" ] ; then
exists=1
break
fi
done
if [ $exists -eq 0 ]; then

# add email to array
wp_emails+=($curr_email)

# and send email if desired
##############################################################################################
# WORDPRESS EMAIL
##############################################################################################
if [[ "$send_mail" == "wp_sendmail" ]]; then

# EDIT THIS SECTION IF YOU WANT TO SEND AN EMAIL TO ALL WORDPRESS CUSTOMERS
# KEEP THE CONTENTS WITHIN A ONE SET OF DOUBLE-QUOTES AND DO NOT USE DOUBLE-QUOTES ANYWHERE
# IN THE MESSAGE ITSELF

echo -e "From: DoIT Shared Hosting <webhosting-notify@lists.wisc.edu>
To: ${array[1]}
Subject: Wordpress instance on ${array[0]}

You have received this message because you are a listed contact for the Shared Web Hosting account with web site ${array[0]}, which hosts a Wordpress instance.

Please be advised of the following Wordpress critical security release: https://wordpress.org/news/2014/11/wordpress-4-0-1/

In addition to following the instructions in the advisory, we strongly recommend that all admininstrators of Wordpress sites turn on automatic updates.

If you have any questions, email webhosting-notify@lists.wisc.edu" |
/usr/sbin/sendmail ${array[1]}

sent="Y"
##############################################################################################

# end sendmail if statement
fi

# end "is this a new email address?" if statement
fi

##############################################################################################
#COMMENT THIS OUT IF YOU DON'T WANT A TAB SEPARATED LIST OF DBS, DOMAINS AND EMAILS

printf "%-35s\t %-50s\t %-45s\t %-7s\t %-9s\t %-11s\n" "$db" "${array[0]}" "${array[1]}" "$version" "$multi" "$sent"

##############################################################################################

#don't loop through multiple tables
break

# end if it's a wordpress db statement
fi

done

done

echo ""
echo "*MULTIPLE means that the database was referenced in multiple wp-config.php files in the domain's web content."
echo "    To see which ones, run:"
echo ""
echo "    sudo find /var/www/vhosts/<DOMAIN NAME>/httpdocs -path \"*wp-config.php\" -exec grep -l \"<DB NAME>\" '{}' \\;"
echo ""

#print comma-separate list of unique emails of WP customers
echo ""
echo "Wordpress customer emails:"
echo ""
for i in "${!wp_emails[@]}"; do
echo "${wp_emails[$i]}"
done

echo ""


##############################################################################################
##############################################################################################
#   DRUPAL
##############################################################################################
##############################################################################################


echo ""
echo "========================"
echo "Drupal DBs on $server"
echo "========================"
echo ""
printf "%-35s\t %-50s\t %-45s\t %-7s\t %-9s\t %-11s\n" "DB NAME" "DOMAIN" "CONTACT" "VERSION" "MULTIPLE*" "EMAIL SENT?"

declare -a drup_emails

for db in $all_dbs
     do
            drup_db="$(mysql -u$user -p$pass $db -sN -e "SHOW TABLES LIKE 'node'")"
                if [[ "$drup_db" == 'node' ]]
                then
                    dbquery="$(mysql -u$user -p$pass psa -sN -e "SELECT dm.displayName, cl.email from
			domains AS dm, clients AS cl, data_bases AS db
			WHERE db.name='$db' AND db.dom_id=dm.id AND dm.cl_id=cl.id")"
		array=( $( for i in $dbquery ; do echo $i ; done ) )

## DETERMINE PATH ####################

#IF PATH RESOLVES
if path=$(find /var/www/vhosts/${array[0]}/httpdocs -path "*/sites/default/settings.php" 2>>$man_logfile -exec grep -l "$db" '{}' \; 2>>$man_logfile | sed 's/\/sites\/default\/settings\.php//'  2>>$man_logfile ); then

#SET multi variable to default of N
multi="N"

#CHECK IF PATH RESOLVES TO MULTIPLE LOCATIONS
if [[ "$path" =~ ".*httpdocs.*var.*" ]]; then
#SET THE MULTI FLAG
multi="Y"

#IF MULTIPLE, SEE IF ONE OF THEM ENDS IN HTTPDOCS
if [[ "$path" =~ ".*httpdocs.{1,2}\/var.*" ]] ||
[[ "$path" =~ "^.*httpdocs$" ]]; then

#IF SO, USE THAT PATH TO DETERMINE VERSION
path="/var/www/vhosts/${array[0]}/httpdocs"

#OTHERWISE, USE THE FIRST PATH IN THE GROUP
else
#path=$(echo $path | sed -n 's/\(.*\).\/var.*/\1/p'  2>>$man_logfile )
path=$(echo $path | sed -n 's/^\(\/var\S*\)\s.*$/\1/p' 2>>$man_logfile )
fi

#IF NOT MULTI, USE PATH TO DETERMINE VERSION
else
path=$path
fi

#IF PATH DID NOT RESOLVE VERSION WILL NOT RESOLVE AND BE UNKNOWN

fi

## DETERMINE VERSION ####################

#IF VERSION RESOLVES BASED ON PATH, USE IT
if version=$(sed -n 's/.*version = \"\(.*\)\".*/\1/p' $path/modules/system/system.info 2>>$man_logfile ); then

version=$(sed -n 's/.*version = \"\(.*\)\".*/\1/p' $path/modules/system/system.info 2>>$man_logfile )

#OTHERWISE, QUESTION MARK
else
version="?"
fi

#build emails list that only includes each customer email one time
sent="N"
curr_email=${array[1]}
exists=0

for i in "${!drup_emails[@]}"; do
if [ "${drup_emails[$i]}" == "$curr_email" ] ; then
exists=1
break
fi
done


if [ $exists -eq 0 ]; then

#add to Drupal customers array
drup_emails+=($curr_email)

# and send email if desired
##############################################################################################
# DRUPAL EMAIL
##############################################################################################

if [[ "$send_mail" == "drupal_sendmail" ]]; then

# EDIT THIS SECTION IF YOU WANT TO SEND AN EMAIL TO ALL DRUPAL CUSTOMERS
# KEEP THE CONTENTS WITHIN A ONE SET OF DOUBLE-QUOTES AND DO NOT USE DOUBLE-QUOTES ANYWHERE
# IN THE MESSAGE ITSELF

echo -e "From: DoIT Shared Hosting <webhosting-notify@lists.wisc.edu>
To: ${array[1]}
Subject: Drupal instance on ${array[0]}

You have received this message because you are a listed contact for the Shared Web Hosting account with web site ${array[0]}, which hosts a Drupal instance.

blah blah blah

If you have any questions, email webhosting-notify@lists.wisc.edu" |
/usr/sbin/sendmail ${array[1]}

sent="Y"
##############################################################################################

# end sendmail if statement
fi

# end "is this a new email address?" if statement
fi

##############################################################################################
#COMMENT THIS OUT IF YOU DON'T WANT A TAB SEPARATED LIST OF DBS, DOMAINS AND EMAILS

printf "%-35s\t %-50s\t %-45s\t %-7s\t %-9s\t %-11s\n" "$db" "${array[0]}" "${array[1]}" "$version" "$multi" "$sent"

##############################################################################################

# end "is it a drupal db?" if statement
fi

     done
echo ""
echo "*MULTIPLE means that the database was referenced in multiple settings.php files in the domain's web content."
echo "    To see which ones, run:"
echo ""
echo "    sudo find /var/www/vhosts/<DOMAIN NAME>/httpdocs -path \"*/sites/default/settings.php\" -exec grep -l \"<DB NAME>\" '{}' \\;"
echo ""

#print comma-separate list of unique emails of Drupal customers
echo ""
echo "Drupal customer emails:"
echo ""
for i in "${!drup_emails[@]}"; do
echo -e "${drup_emails[$i]}"
done

echo ""
