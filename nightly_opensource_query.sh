#!/bin/bash

###############################################################################
###############################################################################
# This script sends a message to the admin email address with the following:
#	1. A list of all DBs on the host that are formatted for WP or Drupal
#	2. The domain name each DB belongs to and the email to contact the customer
#	3. The version of WP or Drupal (where possible)
#	4. A list of any DBs formatted for Drupal or WP created within the past 24 hours
#       5. A list of all customer DBs on the host machine with its domain and customer
#
# It also sends an email to the admin contact for any newly created DBs,
# informing them of the policies and responsibilities of an open source app.
#
# In addition, it creates a .dat file in the /customerlists subdirectory containing
# all the contact emails for the WP DBs discovered.  This .dat file is uploaded each
# night to generate a custom WiscList in the format:
# sharedhosting-wp-<server name>@lists.wisc.edu
#
#  -Ben Sousa 1/28/2016
###############################################################################
###############################################################################

# HOST-SPECIFIC VALUES (CONTAINED IN SEPARATE FILE)
. /usr/local/psa/bin/scripts/host_values

#PRESET VALUES
fulldate=$(/bin/date)
userid=$(/usr/bin/id)
today=`date +%s`
day=$(expr 60 \* 60 \* 24)

### WISCLIST DAT FILE ######################################
wisclist_wp="/usr/local/psa/bin/scripts/customerlists/sharedhosting-wp-${server}.dat"
# make all lowercase
wisclist_wp_dat="$(echo ${wisclist_wp} | tr 'A-Z' 'a-z')"

# remove old version of .dat file and create new one
if [ -f $wisclist_wp_dat ]; then
rm $wisclist_wp_dat
else
touch $wisclist_wp_dat
fi

###LOGGING######################################

# logfile gets a lot of junk, because there are always permissions and not found errors
# as the find and sed commands are executed

#generate header in logfile
echo "========================================================================" >> $logfile 2>> $logfile

# information on the event date and time
echo $fulldate >> $logfile 2>> $logfile

# information on the user, on behalf of which the script was executed (for audit)
echo $userid >> $logfile 2>> $logfile

echo "========================================================================" >> $logfile 2>> $logfile

##############################################################################################

# DECLARE ARRAY TO STORE ALL DB NAMES #############
all_db_names=()

# LIST OF ALL DATABASES #############
all_dbs="$(mysql -u $user -p$pass -Bse 'show databases')"

###########################################################################################
# FIND WORDPRESS DBS 
###########################################################################################
#
# QUERIES FOR DBS WITH WP_OPTIONS TABLE. FOR ANY THAT DO, IT QUERIES THE PSA DB 
# TO FIND THE DOMAIN AND CUSTOMER CONTACT INFO FOR THE DB
#

for db in $all_dbs
do
all_db_names+=($db)
all_tables="$(mysql -u$user -p$pass $db -sN -e "SHOW TABLES LIKE '%_usermeta'" 2>> $logfile)"

for table in $all_tables
do

table_prefix="$(echo $table | sed -n "s/^\(.*\)_usermeta$/\1/p")"
            wp_db="$(mysql -u$user -p$pass $db -sN -e "SELECT meta_key FROM $table WHERE meta_key LIKE '${table_prefix}_%' LIMIT 1" 2>> $logfile)"
                if [ ! -z "$wp_db" ]
         	then
	            dbquery="$(mysql -u$user -p$pass psa -sN -e "SELECT dm.displayName, cl.email FROM 
			domains AS dm, clients AS cl, data_bases AS db 
			WHERE db.name='$db' AND db.dom_id=dm.id AND dm.cl_id=cl.id")"
		
		    array=( $( for i in $dbquery ; do echo $i ; done ) )

## DETERMINE PATH #######################################################################

# IF PATH RESOLVES
#
# LOOK FOR wp-config.php FILE IN WEB CONTENT WITH THE DB'S NAME AND RETURN PATH TO THE FILE |
# REMOVE /wp-config.php FROM THE PATH TO OBTAIN THE ROOT OF THE WP INSTANCE
#

if path=$(find /var/www/vhosts/${array[0]}/httpdocs -path "*/wp-config.php" 2>>$logfile -exec grep -l "$db" '{}' \; 2>>$logfile | sed 's/\/wp-config\.php//'  2>>$logfile ); then

# CHECK IF PATH RESOLVES TO MULTIPLE LOCATIONS
if [[ "$path" =~ ".*httpdocs.*var.*" ]]; then

# IF MULTIPLE, WE NEED TO PICK ONE
# FIRST, SEE IF ONE OF THE PATHS ENDS IN HTTPDOCS

if [[ "$path" =~ ".*httpdocs.{1,2}\/var.*" ]] ||
[[ "$path" =~ "^.*httpdocs$" ]]; then

# IF SO, USE THAT PATH TO DETERMINE VERSION

path="/var/www/vhosts/${array[0]}/httpdocs"

# OTHERWISE, USE THE FIRST PATH FOUND IN THE GROUP

else
path=$(echo $path | sed -n 's/^\(\/var\S*\)\s.*$/\1/p' 2>>$logfile )
fi

# IF NOT MULTI, USE PATH TO DETERMINE VERSION

else 
path=$path
fi

# IF PATH DID NOT RESOLVE, VERSION WILL NOT RESOLVE AND BE UNKNOWN 

fi


## DETERMINE VERSION ##########################################
#
# IF VERSION RESOLVES BASED ON PATH, USE IT
# PULL THE CONTENTS WITHIN THE QUOTES wp_version = ' ' FROM /wp-includes/version.php FILE BASED ON PATH
#

if version=$(sed -n "s/.*wp_version = '\(.*\)'.*/\1/p" $path/wp-includes/version.php 2>>$logfile ); then

version=$(sed -n "s/.*wp_version = '\(.*\)'.*/\1/p" $path/wp-includes/version.php 2>>$logfile )

# OTHERWISE, QUESTION MARK

else 
version="?"
fi

# LOAD DATABASE VALUES INTO WORDPRESS DBS ARRAY ########

wp_dbs+=($db)
wp_domains+=(${array[0]})
wp_contacts+=(${array[1]})
wp_versions+=($version)

# append email to .dat file
echo ";;;${array[1]}" >> $wisclist_wp_dat

### CHECK IF IT'S A NEWLY CREATED DB.  IF SO, SEND WELCOME EMAIL. ##################################################

# PULL DATE VALUE FOR WP USER WITH LOWEST ID ABOVE ZERO

user_query="$(mysql -u$user -p$pass $db -sN -e "SELECT user_registered FROM wp_users  WHERE ID > 0 ORDER BY ID ASC LIMIT 1")"

# PULL DATE VALUE FOR WP POST WITH LOWEST ID ABOVE ZERO

post_query="$(mysql -u$user -p$pass $db -sN -e "SELECT post_date FROM wp_posts WHERE ID > 0 ORDER BY ID ASC LIMIT 1")"

# FIRST CHECK THAT BOTH VALUES WERE OBTAINED AND ARE NON-ZERO.  IF NOT, SKIP SECTION
if [ "$user_query" != "0000-00-00 00:00:00" ] && [ ! -z "$user_query" ] && [ "$post_query" != "0000-00-00 00:00:00" ] && [ ! -z "$post_query" ]; then

# IF SO, CALCULATE HOW LONG AGO IN UNIXTIME

user_created=$(date --date="$user_query" +"%s")
user_result=$(expr $today - $user_created)
post_created=$(date --date="$post_query" +"%s")
post_result=$(expr $today - $post_created)

# IF BOTH VALUES ARE LESS THAN ONE DAY AGO, CONCLUDE IT'S NEWLY CREATED

if [ "$user_result" -le "$day" ] && [ "$post_result" -le "$day" ]; then

# LOAD DB VALUES INTO NEW WP DBS ARRAY #################################

new_wp_dbs+=($db)
new_wp_domains+=(${array[0]})
new_wp_contacts+=(${array[1]})

# SEND WELCOME EMAIL TO CUSTOMER CONTACT ADDRESS ####################################################################

echo -e "From: DoIT Shared Hosting <webhosting-notify@lists.wisc.edu>
To: ${array[1]}
Cc: $admin_email
Subject: Wordpress instance on ${array[0]}

You have received this message because you are a listed contact for the Shared Web Hosting account with web site ${array[0]}, where a new Wordpress instance has been installed.

Open source applications like Wordpress can be very useful, but we want our customers to be aware of the responsibilities and risks associated with them.  Specifically, your Wordpress instance is not part of a centrally managed service like wordpress.com but is yours to maintain.

Wordpress setup is easy, but maintenance requires work.  It is vital for the security of your application that someone be responsible for promptly installing updates to Wordpress and any add-on modules.  Failure to update regularly makes it much more likely that your application will be exploited by a hacker.

For more detail on our policies and recommendations for customers running Wordpress: https://kb.wisc.edu/webhosting/page.php?id=33416

If you have any questions, email webhosting-notify@lists.wisc.edu" |
/usr/sbin/sendmail -t

# MARK AS SENT TO VERIFY IT RAN
                    
new_wp_welcome+=("Sent")

# END "WHETHER IT'S NEW" IF STATEMENT

fi

# END "WHETHER DATE VALUES WERE VALID" IF STATEMENT

fi

# END "IF IT'S A WORDPRESS DB" IF STATEMENT
break
fi

done

done

###########################################################################################
#  FIND DRUPAL DBS
###########################################################################################
#
# QUERIES FOR DBS WITH NODE TABLE. FOR ANY THAT DO, IT QUERIES THE PSA DB
# TO FIND THE DOMAIN AND CUSTOMER CONTACT INFO FOR THE DB
#

for db in $all_dbs
     do
            drup_db="$(mysql -u$user -p$pass $db -sN -e "SHOW TABLES LIKE 'node'")"
                if [[ "$drup_db" == 'node' ]]
                then
                    dbquery="$(mysql -u$user -p$pass psa -sN -e "SELECT dm.displayName, cl.email FROM
                        domains AS dm, clients AS cl, data_bases AS db
                        WHERE db.name='$db' AND db.dom_id=dm.id AND dm.cl_id=cl.id")"

                    array=( $( for i in $dbquery ; do echo $i ; done ) )

## DETERMINE PATH #########################################################################

# IF PATH RESOLVES
#
# LOOK FOR /sites/default/settings.php FILE IN WEB CONTENT WITH THE DB'S NAME AND RETURN PATH TO THE FILE |
# REMOVE /sites/default/settings.php FROM THE PATH TO OBTAIN THE ROOT OF THE DRUPAL INSTANCE
#

if path=$(find /var/www/vhosts/${array[0]}/httpdocs -path "*/sites/default/settings.php" 2>>$logfile -exec grep -l "$db" '{}' \; 2>>$logfile | sed 's/\/sites\/default\/settings\.php//'  2>>$logfile ); then

# CHECK TO SEE IF PATH RESOLVES TO MULTIPLE LOCATIONS

if [[ "$path" =~ ".*httpdocs.*var.*" ]]; then

# IF MULTIPLE, WE NEED TO PICK ONE
# FIRST, SEE IF ONE OF THE PATHS ENDS IN HTTPDOCS

if [[ "$path" =~ ".*httpdocs.{1,2}\/var.*" ]] ||
[[ "$path" =~ "^.*httpdocs$" ]]; then

# IF SO, USE THAT PATH TO DETERMINE VERSION

path="/var/www/vhosts/${array[0]}/httpdocs"

# OTHERWISE, USE THE FIRST PATH IN THE GROUP

else
path=$(echo $path | sed -n 's/^\(\/var\S*\)\s.*$/\1/p' 2>>$logfile )
fi

# IF NOT MULTI, USE PATH TO DETERMINE VERSION

else 
path=$path
fi

# IF PATH DID NOT RESOLVE VERSION WILL NOT RESOLVE AND BE UNKNOWN 

fi

## DETERMINE VERSION #################################################
# 
# IF VERSION RESOLVES BASED ON PATH, USE IT
# PULL THE CONTENTS WITHIN THE QUOTES version = " " FROM /modules/system/system.info FILE BASED ON PATH
#

# IF VERSION RESOLVES BASED ON PATH, USE IT

if version=$(sed -n 's/.*version = \"\(.*\)\".*/\1/p' $path/modules/system/system.info 2>>$logfile ); then

version=$(sed -n 's/.*version = \"\(.*\)\".*/\1/p' $path/modules/system/system.info 2>>$logfile )

# OTHERWISE, QUESTION MARK

else 
version="?"
fi

# LOAD VALUES INTO DRUPAL DBS ARRAY ################################
                    
drupal_dbs+=($db)
drupal_domains+=(${array[0]})
drupal_contacts+=(${array[1]})
drupal_versions+=($version)
                    
### CHECK IF IT'S A NEWLY CREATED DB.  IF SO, SEND WELCOME EMAIL. ##################################################

# PULL DATE VALUE FOR USER WITH LOWEST uid ABOVE ZERO
                
created_query="$(mysql -u$user -p$pass $db -sN -e "SELECT created FROM users WHERE uid > 0 ORDER BY uid ASC LIMIT 1")"

# CHECK IF VALUE IS OBTAINED.  OTHERWISE, SKIP SECTION.
 
if [ ! -z "$created_query" ]; then

# IF SO, CALCULATE HOW LONG AGO IN UNIXTIME

result=$(expr $today - $created_query)

# IF VALUE IS LESS THAN ONE DAY AGO, CONCLUDE IT'S NEWLY CREATED

if [ "$result" -le "$day" ]; then

# LOAD VALUES INTO NEW DRUPAL DBS ARRAY
                    
new_drupal_dbs+=($db)
new_drupal_domains+=(${array[0]})
new_drupal_contacts+=(${array[1]})

# SEND WELCOME EMAIL TO CUSTOMER ADDRESS ################################################

echo -e "From: DoIT Shared Hosting <webhosting-notify@lists.wisc.edu>
To: ${array[1]}
Cc: $admin_email
Subject: Drupal instance on ${array[0]}

You have received this message because you are a listed contact for the Shared Web Hosting account with web site ${array[0]}, where a new Drupal instance has been installed.

Open source applications like Drupal can be very useful, but we want our customers to be aware of the responsibilities and risks associated with them.  Specifically, while your web account is provided by Shared Hosting, your Drupal instance is not part of a centrally managed service but is yours to maintain.

Drupal setup is easy, but maintenance requires work.  It is vital for the security of your application that someone be responsible for promptly installing updates to Drupal and any add-on modules.  Failure to update regularly makes it much more likely that your application will be exploited by a hacker.

For more detail on our policies and recommendations for customers running Drupal: https://kb.wisc.edu/webhosting/page.php?id=33416

If you have any questions, email webhosting-notify@lists.wisc.edu" |
/usr/sbin/sendmail -t

# MARK AS SENT TO CONFIRM IT RAN
                    
new_drupal_welcome+=("Sent")

# END "WHETHER IT'S NEW" IF STATEMENT

fi

# END "WHETHER DATE VALUE WAS VALID" IF STATEMENT

fi

# END "IF IT'S A DRUPAL DB" IF STATEMENT

fi

done


###########################################################################################
# COMPOSE EMAIL TO SHARED HOSTING ADMINS WITH ALL INFO GATHERED
###########################################################################################

# TEMP FILE TO COMPOSE MESSAGE

EMAILMESSAGE="/tmp/emailmessage.txt"

# EMAIL MESSAGE HEADER (SPECIFY HTML) #############################################

printf "From: DoIT Shared Hosting <webhosting-notify@lists.wisc.edu>
To: $admin_email
Subject: Open Source App DBs on $server
Content-Type: text/html; charset=iso-8859-1
MIME-Version: 1.0\n"  > $EMAILMESSAGE

# MESSAGE BODY ####################################################################

printf "<html><head><title></title></head><body style='font-family: Arial'>" >> $EMAILMESSAGE

# IF THERE ARE ANY NEW DBS, INCLUDE THE TABLE FOR THEM ############################

if [ ${#new_wp_dbs[@]} -gt 0 ] || [ ${#new_drupal_dbs[@]} -gt 0 ]; then
    
printf "<font style='font-size:1.4em;font-weight: bold'>New Open Source DBs on $server ($fulldate)</font><br /><br />
<table style='border-spacing: 2 px;border-collapse: separate;'>
<tr><td style='min-width:200px;font-weight: bold'>DB NAME</td>
<td style='min-width:200px;font-weight: bold'>DOMAIN</td>
<td style='min-width:200px;font-weight: bold'>CONTACT</td>
<td style='min-width:100px;font-weight: bold'>WELCOME EMAIL</td></tr>" >> $EMAILMESSAGE

for i in "${!new_wp_dbs[@]}"; do
printf "<tr><td>${new_wp_dbs[$i]}</td><td>${new_wp_domains[$i]}</td><td>${new_wp_contacts[$i]}</td><td>${new_wp_welcome[$i]}</td></tr>" >> $EMAILMESSAGE
done

for i in "${!new_drupal_dbs[@]}"; do
printf "<tr><td>${new_drupal_dbs[$i]}</td><td>${new_drupal_domains[$i]}</td><td>${new_drupal_contacts[$i]}</td><td>${new_drupal_welcome[$i]}</td></tr>" >> $EMAILMESSAGE
done

printf "</table><br /><br />" >> $EMAILMESSAGE

# END "WHETHER THERE ARE NEW DBS" IF STATEMENT #

fi

# LIST ALL WORDPRESS DBS WITH THEIR DOMAIN, CONTACT AND VERSION ################################################

printf "<font style='font-size:1.3em;font-weight: bold'>Wordpress DBs on $server</font><br /><br />

<table style='border-spacing: 2 px;border-collapse: separate;'>
<tr><td style='font-weight:bold'>DB</td>
<td style='font-weight:bold'>DOMAIN</td>
<td style='font-weight:bold'>CONTACT</td>
<td style='font-weight:bold'>VERSION</td></tr>" >> $EMAILMESSAGE

for i in "${!wp_dbs[@]}"; do
printf "<tr><td>${wp_dbs[$i]}</td><td>${wp_domains[$i]}</td><td>${wp_contacts[$i]}</td><td>${wp_versions[$i]}</td></tr>" >> $EMAILMESSAGE
done

# LIST ALL DRUPAL DBS WITH THEIR DOMAIN, CONTACT AND VERSION ###################################################

printf "</table><br /><br />

<font style='font-size:1.3em;font-weight: bold'>Drupal DBs on $server</font><br /><br />

<table style='border-spacing: 2 px;border-collapse: separate;'>
<tr><td style='font-weight:bold'>DB</td>
<td style='font-weight:bold'>DOMAIN</td>
<td style='font-weight:bold'>CONTACT</td>
<td style='font-weight:bold'>VERSION</td></tr>" >> $EMAILMESSAGE

for i in "${!drupal_dbs[@]}"; do
printf "<tr><td>${drupal_dbs[$i]}</td><td>${drupal_domains[$i]}</td><td>${drupal_contacts[$i]}</td><td>${drupal_versions[$i]}</td></tr>" >> $EMAILMESSAGE
done

# LIST ALL CUSTOMER DBS WITH THEIR DOMAIN AND CUSTOMER ################################################

printf "</table><br /><br />
<font style='font-size:1.3em;font-weight: bold'>All Customer DBs on $server</font><br /><br />

<table style='border-spacing: 2 px;border-collapse: separate;'>
<tr><td style='font-weight:bold'>DB</td>
<td style='font-weight:bold'>DOMAIN</td>
<td style='font-weight:bold'>CUSTOMER</td></tr>" >> $EMAILMESSAGE

for i in "${!all_db_names[@]}"; do

db=(${all_db_names[$i]})

                    dbquery="$(mysql -u$user -p$pass psa -sN -e "SELECT CONCAT_WS(':',dm.displayName,cl.pname) as result FROM
                        domains AS dm, clients AS cl, data_bases AS db
                        WHERE db.name='$db' AND db.dom_id=dm.id AND dm.cl_id=cl.id")"

if [ -z "$dbquery" ]
then
continue

else
IFS=':' read -ra array <<< "$dbquery"
printf "<tr><td>$db</td><td>${array[0]}</td><td>${array[1]}</td></tr>" >> $EMAILMESSAGE
fi
done
printf "</table><br /></body></html>" >> $EMAILMESSAGE

# SEND MESSAGE #################################################################

/usr/sbin/sendmail -t < $EMAILMESSAGE
