#!/bin/bash

# Get the directory the script resides in.
SCRIPTPATH=$( cd $(dirname $0) ; pwd -P )

OUTLOG=${SCRIPTPATH}/logs/update_domain_${OLD_DOMAIN_NAME}.log

echo "--------------" >> $OUTLOG 2>> $OUTLOG
echo "--------------domain_updated" >> $OUTLOG 2>> $OUTLOG

# information on the event date and time
/bin/date >> $OUTLOG 2>>$OUTLOG

# information on the user, on behalf of which the script was executed (to ensure control)
/usr/bin/id >> $OUTLOG 2>> $OUTLOG

# information on the created physical hosting account
echo "OLD_DOMAIN_NAME=${OLD_DOMAIN_NAME}" >> $OUTLOG 2>> $OUTLOG
echo "NEW_DOMAIN_NAME=${NEW_DOMAIN_NAME}" >> $OUTLOG 2>> $OUTLOG

# check if the domain name changed
if [ "${OLD_DOMAIN_NAME}" != "${NEW_DOMAIN_NAME}" ]; then
    echo "Domain name changed to ${NEW_DOMAIN_NAME}." >> $OUTLOG 2>> $OUTLOG
    # remove old domain from shibboleth2.xml
    echo "Removing SP for old domain to shibboleth2.xml" >> $OUTLOG 2>> $OUTLOG
    echo "/usr/local/psa/bin/scripts/shib2xml_updater.pl -mode remove -domain ${OLD_DOMAIN_NAME} running" >> $OUTLOG 2>> $OUTLOG
    /usr/local/psa/bin/scripts/shib2xml_updater.pl -mode remove -domain ${OLD_DOMAIN_NAME} >> $OUTLOG 2>> $OUTLOG
    echo "/usr/local/psa/bin/scripts/shib2xml_updater.pl -mode remove -domain ${OLD_DOMAIN_NAME} finished" >> $OUTLOG 2>> $OUTLOG
    # add new domain to shibboleth2.xml
    echo "Adding SP for new domain to shibboleth2.xml" >> $OUTLOG 2>> $OUTLOG
    echo "/usr/local/psa/bin/scripts/shib2xml_updater.pl -mode add -domain ${NEW_DOMAIN_NAME} running" >> $OUTLOG 2>> $OUTLOG
    /usr/local/psa/bin/scripts/shib2xml_updater.pl -mode add -domain ${NEW_DOMAIN_NAME} >> $OUTLOG 2>> $OUTLOG
    echo "/usr/local/psa/bin/scripts/shib2xml_updater.pl -mode add -domain ${NEW_DOMAIN_NAME} finished" >> $OUTLOG 2>> $OUTLOG
else
    echo "Domain name unchanged." >> $OUTLOG 2>> $OUTLOG
fi
