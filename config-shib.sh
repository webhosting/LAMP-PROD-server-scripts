#!/bin/env bash

set -uo pipefail

appname=$(basename $0)
SHIBDCMD="shibd"
OUTPUTFILE="/etc/shibboleth/shibboleth2.xml"
ARCHIVEDIR="/etc/shibboleth/archive"
FORCE_INTERACTIVE=0
DRY_RUN=0
CLEAN=1

function usage {
    cat <<EOF
$appname interactively opens a temporary copy of ${OUTPUTFILE} for
editing, tests the changes made and installs the edits only after
passing static analysis.

Running $appname with no input file will run inteeractively.

Set the \$EDITOR variable to choose an editor

Usage: $appname [-o] [destination] [-s] [input file]

  -o, --output=FILE    destination shibboleth2.xml file [default: ${OUTPUTFILE}]
  -s, --shibd=COMMAND  shibd executable [default: searches \$PATH]
  -i, --interactive    force interactive mode
  -d, --dry-run        don't write to the output file, just print what would have been done
  -n, --no-clean       don't clean up temp files after install
  -h, --help           display this help and exit
EOF
}

function die {
    ERROR=$?
    if [[ ERROR -eq 0 ]]; then
        ERROR=1
    fi
    echo -e "$appname: $1" > /dev/stderr
    exit $ERROR
}

function checkshib {
    if [[ $# -ne 2  ]]; then die "checkshib expects 2 parameters, got $#"; fi
    if [[ ! -r $1 ]]; then die "${1} file not found or not readable"; fi

    echo "checkshib: checking configuration..."
    SHIBDOUT="shibd: $(${SHIBDCMD} -t -c "${1}")"
    STATUS=$?
    # interactive mode
    if [[ $2 -eq 1 ]]; then
        return $STATUS
    fi
    if [[ $STATUS -ne 0 ]]; then
        die "${SHIBDOUT}\n\nERROR: validation failed. Configuration not installed"
    fi
}


ARGS=()
while [[ $# -gt 0 ]]; do
    case $1 in
        -o|--output)
            if [[ ! -f $2 ]]; then die "$2 file not found"; fi
            OUTPUTFILE=$2
            shift 2
            ;;
        -s|--shibd)
            if [[ ! -f $2 ]]; then die "$2 file not found"; fi
            if [[ ! -x $2 ]]; then die "$2 cannot execute"; fi
            SHIBDCMD=$2
            ;;
        -i|--interactive)
            FORCE_INTERACTIVE=1
            shift 1
            ;;
        -d|--dry-run)
            DRY_RUN=1
            shift 1
            ;;
        -n|--no-clean)
            CLEAN=0
            shift 1
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        -*|--*)
            die "invalid option $1"
            ;;
        *)
            ARGS+=("${1}")
            shift
            ;;
    esac
done

USERID=$(id -u)
INTERACTIVE=1

# command is called with an input file which assumes non-interactive usage
if [[ ${#ARGS[@]} -gt 1 ]]; then
    die "extra operands ${ARGS[@]:1}"
elif [[ ${#ARGS[@]} -eq 1 && $FORCE_INTERACTIVE -ne 1 ]]; then
    if [[ $USERID -ne 0 ]]; then die "non-interactive usage requires $appname to be run as root"; fi
    INTERACTIVE=0
fi

RED=""
GREEN=""
YELLOW=""
NORMAL=""
CLEAR=""

if [[ $INTERACTIVE -eq 1 ]]; then
    RED=$(tput setaf 1)
    GREEN=$(tput setaf 2)
    YELLOW=$(tput setaf 3)
    NORMAL=$(tput sgr0)
    CLEAR=$(tput clear)
fi

function colorize { 
    while IFS= read -r line; do
        echo $(echo "${line}" | sed "s/^\(-[^-].*\)$/$RED\1$NORMAL/" | sed "s/^\(+[^+].*\)$/$GREEN\1$NORMAL/")
    done <<< "${1}";
}

INPUTFILE=""
if [[ ${#ARGS[@]} -eq 1 ]]; then
    INPUTFILE="${ARGS[0]}"
    if [[ ! -r  "${INPUTFILE}" ]]; then die "input file not found or not readable"; fi
fi

# ensure shibd is available
if [[ -z "$SHIBDCMD" ]]; then
    SHIBDCMD=$(which shibd || die "shibd not found on path. ")
    if [[ ! -x $SHIBDCMD ]]; then die "$SHIBDCMD must be executable"; fi
fi

if [[ ! -r $OUTPUTFILE ]]; then
    die "${OUTPUTFILE} file not found or not readable"
fi

ARCHIVECMD=()
CMD=()
if [[ $USERID -ne 0 ]]; then
    CMD+=("sudo")
    ARCHIVECMD+=("sudo")
fi
CMD+=("install" "--mode" "644")

CLEANCMD=""
if [[ $INTERACTIVE -eq 1 ]]; then
    TEMPFILE=$(mktemp --suffix=-shibboleth2.xml)
    CLEANCMD="rm ${TEMPFILE}"
    cat "${OUTPUTFILE}" > "${TEMPFILE}"

    DIFFCMD=("diff" "-u3")
    HACKCOLORS=0
    if [[ $(diff --help) =~ "--color" ]]; then
        DIFFCMD+=("--color=always")
    else
        HACKCOLORS=1
    fi

    DIFFCMD+=("${OUTPUTFILE}" "${TEMPFILE}")

    DIFFSTATUS=0

    while true; do
        ${EDITOR:-vim} "${TEMPFILE}"
        DIFFOUT=$(${DIFFCMD[@]});
        DIFFSTATUS=$?
        if [[ $HACKCOLORS -eq 1 ]]; then
            DIFFOUT=$(colorize "${DIFFOUT}")
        fi
        checkshib "${TEMPFILE}" $INTERACTIVE
        if [[ $? -ne 0 ]]; then
            echo -e "================ DIFF ================\n${DIFFOUT}\n"
            echo -e "================ SHIBD ================\n${SHIBDOUT}\n"
            while true; do
                echo -e -n "${RED}ERROR: validation failed!${NORMAL}\nWould you like to edit again and try to resolve above errors? (y/n): "
                read 
                case $REPLY in
                    y | Y) echo -e -n $CLEAR; break;; # clear screen and back to the edit loop
                    n | N) die "${YELLOW}Configuration not installed. exiting.${NORMAL}"; break;;
                    *) # invalid, ask again
                    ;;
                esac
            done
        else
            break
        fi
    done

    
    if [[ $DIFFSTATUS -eq 0 ]]; then
       echo "diff: No edits made. Exiting" > /dev/stderr
       exit 0
    fi

    APPLY=0
    while true; do
        echo -e "================ DIFF ================\n${DIFFOUT}\n"
        echo -n "Apply changes to ${OUTPUTFILE}? (y/n): "
        read 
        case $REPLY in
            y | Y) APPLY=1; break;;
            n | N) APPLY=0; break;;
            *) # invalid, ask again
            ;;
        esac
    done

    if [[ $APPLY -eq 0 ]]; then
        echo "${YELLOW}INFO: Configuration not updated${NORMAL}" > /dev/stderr
        exit 0
    fi
    CMD+=("${TEMPFILE}")
else
    CMD+=("${INPUTFILE}")
fi

ARCHIVE="${ARCHIVEDIR}/shibboleth2.xml.$(date +%Y%m%d_%H%M%S)"
ARCHIVECMD+=("cp -a ${OUTPUTFILE} ${ARCHIVE}")
echo "running archive command:"
echo "${ARCHIVECMD[@]}"
if [[ $DRY_RUN -eq 0 ]]; then
    ${ARCHIVECMD[@]} || die "failed to archive original file"
fi

CMD+=("${OUTPUTFILE}")
echo "running install command:"
echo "${CMD[@]}"
if [[ $DRY_RUN -eq 0 ]]; then
    ${CMD[@]} || die "failed to install ${CMD[@]}"
fi

if [[ $CLEAN -eq 1 && -n $CLEANCMD && DRY_RUN -eq 0 ]]; then
    echo "cleaning up"
    echo "${CLEANCMD}"
    $CLEANCMD || die "unable to clean up"
fi

if [[ $INTERACTIVE -eq 1 ]]; then
    echo -e "${GREEN}OK${NORMAL}"
fi

exit 0
