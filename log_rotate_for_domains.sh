#!/bin/bash

#To run this script, just provide it with a single parameter:
#the path to a file with a domain on each line.  The script
#will run the plesk command to enable log rotation for each 
#domain. -  Ben Sousa 4/21/2014

# Declare array for domains
declare -a ARRAY
# Link filedescriptor 10 with stdin
exec 10<&0
# stdin replaced with a file supplied as a first argument
exec < $1
let count=0

while read LINE; do

    ARRAY[$count]=$LINE
    ((count++))
done

#Set a while loop up to the number of domains in the array,
#run the enable log rotation command for each domain.

         COUNTER=0
         while [  $COUNTER -lt ${#ARRAY[@]} ]; do
        echo ${ARRAY[$COUNTER]}
	/usr/local/psa/bin/site -u ${ARRAY[$COUNTER]} -log_rotate true
            let COUNTER=COUNTER+1
         done

# restore stdin from filedescriptor 10
# and close filedescriptor 10
exec 0<&10 10<&-
