#!/bin/bash
# Written by Phil Jochimsen, 1/24/2022
#
# List things being monitored:
# sudo plesk ext monitoring --thresholds --list
#
# Update /var/log/audit to only alert over 100% (aka: never send email alert, but keep graph over time)
sudo plesk ext monitoring --thresholds --set -panel-id hdd_util__var-log-audit -target df-var-log-audit:df_complex-used:value -operator greater_than -type percent -value 100
# Output updated setting
sudo plesk ext monitoring --thresholds --list | grep audit
#
# Alternatively, Disable /var/log/audit completely:
# sudo plesk ext monitoring --thresholds --unset -panel-id hdd_util__var-log-audit
