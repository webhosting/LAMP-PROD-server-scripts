<?php

// All db connection variables are contained in the required db_conns.php.inc file
require __DIR__ . '/db_conns.php.inc';

// Set constants with locations of footer files
define("HTML_FTR_LOCATION", __DIR__ .'MessageFtrHtml.html');
define("PLAIN_FTR_LOCATION", __DIR__ .'MessageFtrPlain');

// Get text for custom footers instead of default footer that allows users to unsubscribe

// HTML footer version
if (is_readable(HTML_FTR_LOCATION)) {
    $messageFtrHtml = file_get_contents(HTML_FTR_LOCATION);
}
else {
$messageFtrHtml = "";
}

// Plain text footer version
if (is_readable(PLAIN_FTR_LOCATION)) {
    $messageFtrPlain = file_get_contents(PLAIN_FTR_LOCATION);
}
else {
$messageFtrPlain = "";
}

// Set error handling
////////////////////////

define( "ERROR_EMAIL", "webhosting-notify@lists.wisc.edu" );
$notifyEmail = "webhosting-notify@lists.wisc.edu";

error_reporting( E_ALL );
set_error_handler( "errHandler", E_ALL );
set_exception_handler( "exceptionHandler" );

// Some variable initializations
//////////////////////////////////

$newLists = array();
$newContacts = array();
$deletedContacts = array();
$deletedLists = array();
$updatedMembers = array();

$contactType = array( 1 => "ADMIN", 2 => "TECHNICAL" );

// Determine if test or production
////////////////////////////////////

$isProduction = false;
if ( in_array( "-p", $argv ) ) $isProduction = true;

if ( $isProduction ) {
    echo "Working in PRODUCTION environment.\n";
//    $notifyEmail .= ",webhosting-notify@lists.wisc.edu";
} else {
    echo "Working in TEST environment.\n";

}

// Destroy Everything
//////////////////////

$nukeEverything = false;
if ( in_array( "NUKE", $argv ) ) $nukeEverything = true;

if ( $nukeEverything ) echo "\n\n===== I HOPE YOU KNOW WHAT YOU'RE DOING =====\n\n";

// Load correct db connection values for environment from variables in db_conns.php.inc
if ( $isProduction ) {

    $lyrisDB = $lyrisProdDB;
    $lyrisUser = $lyrisProdUser;
    $lyrisPassword = $lyrisProdPassword;

    // Use same DB as List Manager
    $cleanupDB = $lyrisDB;
    $cleanupPassword = $cleanupProdPassword;

    $soapHost = $soapProdHost;
    $soapUser = $soapProdUser;
    $soapPassword = $soapProdPassword;
}

else {


    $lyrisDB = $lyrisTestDB;
    $lyrisUser = $lyrisTestUser;
    $lyrisPassword = $lyrisTestPassword;

    // Use same DB as List Manager
    $cleanupDB = $lyrisDB;
    $cleanupPassword = $cleanupTestPassword;

    $soapHost = $soapTestHost;
    $soapUser = $soapTestUser;
    $soapPassword = $soapTestPassword;

}

// Connect to webhosting database
////////////////////////////////////

$dbhDWH = oci_connect( $dwhUser, $dwhPassword, $dwhDB );
echo "Connected to $dwhDB as user $dwhUser.\n";

// Connect to ListManager database
////////////////////////////////////

$dbhLyris = oci_connect( $lyrisUser, $lyrisPassword, $lyrisDB );
echo "Connected to $lyrisDB as user $lyrisUser.\n";

// Connect to WiscList Cleanup DB
///////////////////////////////////

$dbhCleanup = oci_connect( $cleanupUser, $cleanupPassword, $cleanupDB );
echo "Connected to $cleanupDB as user $cleanupUser.\n";

// Connect to Lyris SOAP
///////////////////////////

$soap = new SoapClient( $soapHost, array( "login" => $soapUser, "password" => $soapPassword ) );
$apiVersion = $soap->ApiVersion();

echo "Connected to ListManager API: $apiVersion\n";

echo "\n";

// Prepared Queries for Loops
//////////////////////////////

// Lookup existence of list

$query = "
SELECT      COUNT(NAME_)
FROM        LISTS_
WHERE       NAME_ = :list
";

$sthFindList = oci_parse( $dbhLyris, $query );

// Lookup existence of list in WiscList Cleanup

$query = "
SELECT      COUNT(LIST_NAME)
FROM        LISTS
WHERE       LIST_NAME = :list";

$sthFindCleanupList = oci_parse( $dbhCleanup, $query);

// Check if list is exempt in WiscList Cleanup

$query = "
SELECT      COUNT(LIST_NAME)
FROM        LISTS
WHERE       LIST_NAME = :list
            AND EXEMPT = 'T'";

$sthFindCleanupExempt = oci_parse( $dbhCleanup, $query);

// Make list exempt in WiscList Cleanup

$query = "
UPDATE      LISTS
SET         EXEMPT = 'T'
WHERE       LIST_NAME = :list";

$sthUpdateCleanupExempt = oci_parse( $dbhCleanup, $query);

// Get Contacts for account

$query = "
SELECT      PERSONS.LNAME,
            PERSONS.FNAME,
            PERSONS.EMAIL,
            CONTACTS.CONTYPE,
            CONTACTS.CONTACTID
FROM        PERSONS, CONTACTS
WHERE       CONTACTS.ACCOUNTID = :acct
            AND ( CONTACTS.CONTYPE = 1 OR CONTACTS.CONTYPE = 2 )
            AND CONTACTS.PERSONID = PERSONS.PERSONID
ORDER BY    CONTACTS.CONTYPE ASC, PERSONS.LNAME ASC, PERSONS.FNAME ASC
";

$sthContacts = oci_parse( $dbhDWH, $query );

// Look for member in list

$query = "
SELECT      MEMBERID_,
            EMAILADDR_,
            FULLNAME_
FROM        MEMBERS_
WHERE       LIST_ = :list
            AND USERID_ = :userid
";

$sthFindMember = oci_parse( $dbhLyris, $query );

// Update Member with User ID

$query = "
UPDATE      MEMBERS_
SET         USERID_ = :userid
WHERE       MEMBERID_ = :memberid
";

$sthUpdateUserID = oci_parse( $dbhLyris, $query );

// Update Member Name

$query = "
UPDATE      MEMBERS_
SET         FULLNAME_ = :name
WHERE       MEMBERID_ = :memberid
";

$sthUpdateMemberName = oci_parse( $dbhLyris, $query );

// Get Members on list

$query = "
SELECT      MEMBERID_, USERID_, EMAILADDR_
FROM        MEMBERS_
WHERE       LIST_ = :list
            AND ( USERNAMELC_ != 'webhosting' OR DOMAIN_ != 'doit.wisc.edu' )
            AND USERNAMELC_ != 'noreply'
ORDER BY    EMAILADDR_ ASC
";

$sthGetMembers = oci_parse( $dbhLyris, $query );

// Set list setting to not visible, set it as a child list: http://lunar.lyris.com/help/lm_help/12.0/Content/listbasicsenable.html
// and set custom plain text and HTML footer
$query = "
UPDATE      LISTS_
SET         MRIVISIBILITY_ = 'H',
        CHILD_    = 'T',
        ALLOWDUPE_  = 'F',
        ALLOWCROSS_ = 'T',
        CROSSCLEAN_ = 'T',
        NOARCHIVE_  = 'F',
        MESSAGEFTR_ = :msgFtrPlain,
        DIGESTFTR_ = :msgFtrPlain,
        MESSAGEFTRHTML_ = :msgFtrHTML
WHERE       LISTID_ = :id
";


$sthMRI = oci_parse( $dbhLyris, $query );

// Get all accounts in webhosting that are active
///////////////////////////////////////////////////

$query = "
SELECT      ACCOUNTS.ACCOUNTID,
            ACCOUNTS.USERID,
            ACCOUNTS.FULLNAME
FROM        ACCOUNTS, PROPERTIES
WHERE       ACCOUNTS.ACCSTAT = 4
            AND ACCOUNTS.ACCOUNTID = PROPERTIES.ACCOUNTID
            AND ( SELECT COUNT(*) FROM PROPERTIES
                  WHERE PROPERTIES.ACCOUNTID = ACCOUNTS.ACCOUNTID
                  AND PROPERTIES.INVSRC = 'WEBHOST' ) > 0
ORDER BY    ACCOUNTS.USERID ASC
";

$sthAcct = oci_parse( $dbhDWH, $query );
oci_execute( $sthAcct );
$accounts = array();
$seen = array();

while ( $acct = oci_fetch_array( $sthAcct ) ) {
    if ( !in_array( $acct["USERID"], $seen ) ) {
        $accounts[] = $acct;
        $seen[] = $acct["USERID"];
    }
}

oci_free_statement( $sthAcct );

// Get all lists that start with dwh-
/////////////////////////////////////

$query = "
SELECT      NAME_, LISTID_
FROM        LISTS_
WHERE       NAME_ LIKE 'dwh-%'
ORDER BY    NAME_ ASC
";

$sthLists = oci_parse( $dbhLyris, $query );
oci_execute( $sthLists );
$lists = array();

while ( $list = oci_fetch_array( $sthLists ) ) {
    $lists[] = $list;
}

oci_free_statement( $sthLists );

// Cleanup lists
/////////////////
echo "Cleaning up lists.\n";
foreach( $lists as $list ) {
    $deleteList = true;

    if ( !$nukeEverything ) {
        foreach( $accounts as $acct ) {
            $listname = strtolower( "dwh-" . $acct["USERID"] );
            if ( $listname == $list["NAME_"] ) $deleteList = false;
        }
    }

    if ( $deleteList or $nukeEverything ) {
        $soap->DeleteList( $list["NAME_"] );
        echo "Deleted " . $list["NAME_"] . ".\n\n";
        $deletedLists[] = $list["NAME_"];
    }

}

if ( $nukeEverything ) {
    echo "\n\nAnd only a barren wasteland remains...\n\n";
    exit(666);
}

// Update existing lists
///////////////////////////

foreach( $accounts as $acct ) {

    echo "==========\n\n";
    echo "Account: " . $acct["FULLNAME"] . "\n";
    echo "User ID: " . $acct["USERID"] . "\n";
    echo "Account ID: " . $acct["ACCOUNTID"] . "\n";

    $listname = strtolower( "dwh-" . $acct["USERID"] );
    echo "List Name: $listname\n";

    // See if list exists in ListManager
    //////////////////////////////////////

    oci_bind_by_name( $sthFindList, ":list", $listname );
    oci_execute( $sthFindList );
    $listCount = oci_fetch_array( $sthFindList );

    if ( $listCount[0] > 0 ) {
        echo "   List exists in ListManager\n";

        // See if list exists in WiscList Cleanup
        ///////////////////////////////////////////

        oci_bind_by_name( $sthFindCleanupList, ":list", $listname );
        oci_execute( $sthFindCleanupList );
        $listCleanupCount = oci_fetch_array( $sthFindCleanupList );

        // If the list is not exempt, set exempt status
        if ( $listCleanupCount[0] > 0 ) {

            oci_bind_by_name( $sthFindCleanupExempt, ":list", $listname );
            oci_execute( $sthFindCleanupExempt );
            $listCleanupExemptCount = oci_fetch_array( $sthFindCleanupExempt );

            if ( $listCleanupExemptCount[0] > 0 ) {
                echo "   List does exists in the WiscList Cleanup DB, and is already exempt from
cleanup.\n";
            } else {

                // Exempt list
                oci_bind_by_name( $sthUpdateCleanupExempt, ":list", $listname );
                oci_execute($sthUpdateCleanupExempt);

                echo "   List has been updated in the WiscList Cleanup database to be exempt from
cleanup.\n";
            }

        } else {
            echo "   List does NOT exist in the WiscList Cleanup DB, I'll check back next time the
script runs.\n";
        }

    } else {

        echo "   List DOES NOT exist in ListManager\n";

        // Create List in Listmanager
        $listID = $soap->CreateList( "discussion-unmoderated", $listname, "DoIT Shared Hosting Contacts for
".$acct["FULLNAME"], "DoIT Shared Hosting",
"webhosting-notify@lists.wisc.edu", genRandomString(), "main" );

        // Update settings
    oci_bind_by_name($sthMRI,":msgFtrPlain",$messageFtrPlain);
    oci_bind_by_name($sthMRI,":msgFtrHTML",$messageFtrHTML);
        oci_bind_by_name( $sthMRI, ":id", $listID );
        oci_execute( $sthMRI );

        echo "   Created list $listname with List ID: $listID\n";
        $newLists[] = array( "listname" => $listname, "accountName" => $acct["FULLNAME"], "accountID" =>
$acct["ACCOUNTID"] );

    }

    // Get Contacts from hosting DB
    ////////////////////////////////

    oci_bind_by_name( $sthContacts, ":acct", $acct["ACCOUNTID"] );
    oci_execute( $sthContacts );
    $contacts = array();

    while ( $contact = oci_fetch_array( $sthContacts ) ) {
        $contacts[] = $contact;
    }

    // Get Members from List
    ///////////////////////////

    oci_bind_by_name( $sthGetMembers, ":list", $listname );
    oci_execute( $sthGetMembers );
    $members = array();

    while ( $member = oci_fetch_array( $sthGetMembers ) ) {
        $members[] = $member;
    }

    // Cleanup Members
    ////////////////////////

    foreach( $members as $member ) {

        $deleteMember = true;
        foreach( $contacts as $contact ) {
            if ( $member["USERID_"] == $contact["CONTACTID"] ) $deleteMember = false;
        }

        if ( $deleteMember ) {
            $soap->DeleteMembers( array( "MemberID = " . $member["MEMBERID_"] ) );
            echo "   Deleted member: " . $member["EMAILADDR_"] . "\n";
            $deletedContacts[] = array( "list" => $listname, "email" => $member["EMAILADDR_"] );
        }

    }

    // Add / Update contacts
    //////////////////////////

    foreach( $contacts as $contact ) {

        $contactName = $contact["FNAME"] . " " . $contact["LNAME"] . " (" . $contactType[$contact["CONTYPE"]] . ")";
        $contactEmail = strtolower( $contact["EMAIL"] );

        echo "   $contactName - $contactEmail [" . $contact["CONTACTID"] . "]\n";

        // Find member in list
        oci_bind_by_name( $sthFindMember, ":list", $listname );
        oci_bind_by_name( $sthFindMember, ":userid", $contact["CONTACTID"] );
        oci_execute( $sthFindMember );
        $member = oci_fetch_array( $sthFindMember );

        if ( $member === false ) {

            echo "      Contact not found in ListManager!\n";

            // Create Member
            /////////////////

            $newMemberID = $soap->CreateSingleMember( $contactEmail, $contactName, $listname );

            // Update Member with User ID
            //////////////////////////////

            oci_bind_by_name( $sthUpdateUserID, ":userid", $contact["CONTACTID"] );
            oci_bind_by_name( $sthUpdateUserID, ":memberid", $newMemberID );
            oci_execute( $sthUpdateUserID );

            echo "      Created Member with ID: $newMemberID\n";

            $newContacts[] = array( "listname" => $listname, "account" => $acct["FULLNAME"], "name" => $contactName,
"email" => $contactEmail );

        } else {

            echo "      Found member: " . $member["MEMBERID_"] . "\n";

            // Update Email
            /////////////////

            if ( $contactEmail != $member["EMAILADDR_"] ) {

                $soap->UpdateMemberEmail( array( "MemberID" => $member["MEMBERID_"] ), $contactEmail );
                echo "      Email address changed: $contactEmail\n";
                $updatedMembers[] = array( "listname" => $listname, "account" => $acct["FULLNAME"], "contact" =>
$contactName, "change" => "New email: " . $contactEmail
);

            }

            // Update Name
            ///////////////

            if ( $contactName != $member["FULLNAME_"] ) {

                oci_bind_by_name( $sthUpdateMemberName, ":name", $contactName );
                oci_bind_by_name( $sthUpdateMemberName, ":memberid", $member["MEMBERID_"] );
                oci_execute( $sthUpdateMemberName );

                echo "      Contact name changed: $contactName\n";

                $updatedMembers[] = array( "listname" => $listname, "account" => $acct["FULLNAME"], "contact" =>
$contactName, "change" => "New name"  );

            }

        }

    }

    echo "\n";

//Add noreply addresses from Plesk to lists
////////////////////////////////////////============================/////////////////////////////////////////////
    $pleskaddresses = array(
        array("servername" => "Boris","email" => "noreply@linux3.dwh.doit.wisc.edu"),
        array("servername" => "Bullwinkle","email" => "noreply@secure.linux.dwh.doit.wisc.edu"),
        array("servername" => "Bugs","email" => "noreply@secure.linux2.dwh.doit.wisc.edu"),
        array("servername" => "Cranium","email" => "noreply@secure.win.dwh.doit.wisc.edu"),
        array("servername" => "Incus","email" => "noreply@secure.win.dwht.doit.wisc.edu"),
        array("servername" => "Natasha","email" => "noreply@linux2.dwht.doit.wisc.edu"),
        array("servername" => "Patella","email" => "noreply@win2.dwht.doit.wisc.edu"),
        array("servername" => "Palatine","email" => "noreply@win3.dwht.doit.wisc.edu"),
        array("servername" => "Rocky","email" => "noreply@secure.linux.dwht.doit.wisc.edu"),
        array("servername" => "Daffy","email" => "noreply@secure.linux2.dwht.doit.wisc.edu"),
	array("servername" => "Sternum","email" => "noreply@win3.dwh.doit.wisc.edu"),
        array("servername" => "Maxilla","email" => "noreply@win4.dwh.doit.wisc.edu"),
        array("servername" => "Ponsonby","email" => "noreply@linux4.dwh.doit.wisc.edu"),
        array("servername" => "Pepe","email" => "noreply@linux6.dwh.doit.wisc.edu"),
	array("servername" => "Cloyd","email" => "noreply@linux3.dwht.doit.wisc.edu"),
	array("servername" => "Aesop","email" => "noreply@linux5.dwh.doit.wisc.edu"),
	array("servername" => "Lunate","email" => "noreply@secure2.win.dwht.doit.wisc.edu"),
	array("servername" => "Hamate","email" => "noreply@secure2.win.dwh.doit.wisc.edu")
	
      );

      // Look for noreply member in list

      $noreplyQuery = "
      SELECT      MEMBERID_,
                  EMAILADDR_,
                  FULLNAME_
      FROM        MEMBERS_
      WHERE       LIST_ = :list
                  AND EMAILADDR_ = :emailadd
      ";

      $sthFindNoReply = oci_parse( $dbhLyris, $noreplyQuery );

      // Update Noreply Member

      $updateNoreplyQuery = "
      UPDATE      MEMBERS_
      SET         USERNAMELC_ = :lcname
      WHERE       MEMBERID_ = :memberid
      ";

      $sthUpdateNoReply = oci_parse( $dbhLyris, $updateNoreplyQuery );

      echo "Handling Noreply emails for Plesk.\n";

    //Loop through noreply addresses to see if they exist in the list and, if not, add them
    ///////////////////////
    foreach( $pleskaddresses as $pleskaddress ) {

        $pleskName = "noreply";
        $pleskEmail = strtolower( $pleskaddress["email"] );

        // Find member in list
        oci_bind_by_name( $sthFindNoReply, ":list", $listname );
        oci_bind_by_name( $sthFindNoReply, ":emailadd", $pleskEmail );
        oci_execute( $sthFindNoReply );
        $member = oci_fetch_array( $sthFindNoReply );

        if ( $member === false ) {

            echo "      Contact not found in ListManager!\n";

            // Create Noreply Member
            /////////////////

            $newMemberID = $soap->CreateSingleMember( $pleskEmail, $pleskName, $listname );

            // Update Noreply Member with User ID
            //////////////////////////////

            $username = "noreply";

            oci_bind_by_name( $sthUpdateNoReply, ":lcname", $username );
            oci_bind_by_name( $sthUpdateNoReply, ":memberid", $newMemberID );
            oci_execute( $sthUpdateNoReply );

            echo "      Created Member with ID: $newMemberID\n";

            $newContacts[] = array( "listname" => $listname, "account" => $acct["FULLNAME"], "name" => $pleskName, "email" => $pleskEmail );

        } else {

            echo "      Found member: " . $member["MEMBERID_"] . "\n";

            // Update Email
            /////////////////

            if ( $pleskEmail != $member["EMAILADDR_"] ) {

                $soap->UpdateMemberEmail( array( "MemberID" => $member["MEMBERID_"] ), $pleskEmail );
                echo "      Email address changed: $pleskEmail\n";
                $updatedMembers[] = array( "listname" => $listname, "account" => $acct["FULLNAME"], "contact" =>
    $pleskName, "change" => "New email: " . $pleskEmail
    );
        }
    }
}

////////////////////////////////////////============================/////////////////////////////////////////////

}

// Close statement handlers
//////////////////////////////

oci_free_statement( $sthFindList );
oci_free_statement( $sthContacts );
oci_free_statement( $sthFindMember );
oci_free_statement( $sthUpdateUserID );
oci_free_statement( $sthUpdateMemberName );
oci_free_statement( $sthGetMembers );
oci_free_statement( $sthMRI );
oci_free_statement( $sthFindNoReply );
oci_free_statement( $sthUpdateNoReply );

// Make sure webhosting@doit is set to nomail
////////////////////////////////////////////////

$query = "
UPDATE      MEMBERS_
SET         SUBTYPE_ = 'nomail'
WHERE       (
                ( USERNAMELC_ = 'webhosting'
                AND DOMAIN_ = 'doit.wisc.edu' )
            OR ( USERNAMELC_ = 'noreply' )
            )
            AND LIST_ LIKE 'dwh-%'
            AND USERID_ IS NULL
";

$sth = oci_parse($dbhLyris, $query);
oci_execute( $sth );


// Close resource handlers
////////////////////////////

oci_close( $dbhDWH );
echo "Closed connection to $dwhDB.\n";

oci_close( $dbhLyris );
echo "Closed connection to $lyrisDB.\n";

// Send Mail Out
///////////////////

$message = "Change summary for customerWiscLists.php:\n";
$message .= "=========================================\n\n";

$message .= "New Lists: " . sizeof( $newLists ) . "\n";
foreach( $newLists as $curr ) {
    $message .= "   " . $curr["listname"] . " - " . $curr["accountName"] . " (ID: " . $curr["accountID"] . ")\n";
}
$message .= "\n";

$message .= "New Contacts: " . sizeof( $newContacts ) . "\n";
foreach( $newContacts as $curr ) {
    $message .= "   " . $curr["listname"] . ": " . $curr["email"] . " - " . $curr["name"] . "\n";
}
$message .= "\n";

$message .= "Updated Members: " . sizeof( $updatedMembers ) . "\n";
foreach( $updatedMembers as $curr ) {
    $message .= "   " . $curr["listname"] . ": " . $curr["contact"] . " - " . $curr["change"] . "\n";
}
$message .= "\n";

$message .= "Deleted Lists: " . sizeof( $deletedLists ) . "\n";
foreach( $deletedLists as $curr ) {
    $message .= "   $curr\n";
}
$message .= "\n";

$message .= "Deleted Contacts: " . sizeof( $deletedContacts ) . "\n";
foreach( $deletedContacts as $curr ) {
    $message .= $curr["list"] . ": " . $curr["email"] . "\n";
}
$message .= "\n";

$message .= php_uname();

echo "\n\nmessage=[$message] \n\n Sent to $notifyEmail\n ";
echo mail( $notifyEmail, "Shared Hosting Customer WiscList Update Summary", $message ) ? "success":"fail"."\n\n";

// Error Handlers
///////////////////

function errHandler( $errno, $errstr, $errFile, $errLine ) {

    $hostname = php_uname( "n" );
    $subject = "Error encountered in " . $_SERVER["SCRIPT_NAME"] . " ($hostname)";

    $message = $errstr . "\n\n";
    $message .= "Line number:  $errLine\n";
    $message .= php_uname();

    mail( ERROR_EMAIL, $subject, $message, "X-Priority: 1" );

    echo "\n\nError ->" . $errstr . " on line:" . $errLine . "\n\n";
    exit(1);

}

function exceptionHandler( $exception ) {

    $hostname = php_uname( "n" );
    $subject = "Error encountered in " . $_SERVER["SCRIPT_NAME"] . " ($hostname)";

    $message = $exception->getMessage() . "\n\n";
    $message .= "Line number: " . $exception->getLine() . "\n";
    $message .= php_uname();

    mail( ERROR_EMAIL, $subject, $message, "X-Priority: 1" );

    echo "\n\nException -> " . $exception->getMessage() . "on line " . $exception->getLine() . "\n\n";
    exit(1);

}

// Random string generator for passwords
///////////////////////////////////////////

function genRandomString() {
    $length = 10;
    $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
    $string = "";

    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters)-1)];
    }

    return $string;
}

?>
