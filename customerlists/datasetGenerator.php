<?php

// All db connection variables are contained in the required db_conns.php.inc file
require __DIR__ . '/db_conns.php.inc';

define ( "SENDTO", "webhosting-notify@lists.wisc.edu");

error_reporting( E_ERROR | E_WARNING );
ini_set( "display_errors", true );

set_error_handler( "myErrorHandler", E_ERROR | E_WARNING );
  
$datasets = array(
     array(
           "platform" => "Linux Platform",
           "string" => "177",
           "query" => "1",
           "file" => "sharedhosting-linux.dat",
           "num" => 0),
    array(
           "platform" => "Windows Platform", 
           "string" => "182",
           "query" => "1",
           "file" => "sharedhosting-win-new.dat",
           "num" => 0),
    array(
           "platform" => "Java Platform",
           "string" => "178",
           "query" => "1",
           "file" => "sharedhosting-java.dat",
           "num" => 0),
    array(
           "platform" => "All Platforms",
           "string" => "(SELECT DISTINCT PROPERTIES.ACCOUNTID FROM PROPERTIES WHERE PROPERTIES.INVSRC = 'WEBHOST')",
           "query" => "2",
           "file" => "sharedhosting-all.dat",
           "num" => 0),
    array(
           "platform" => "Restricted Data Platform",
           "string" =>  "(SELECT DISTINCT PROPERTIES.ACCOUNTID FROM PROPERTIES WHERE PROPERTIES.TITLE = 'Restricted Data')",
           "query" => "2",
           "file" => "sharedhosting-restricted-data.dat",
           "num" => 0),
    );


// Connect to Database
////////////////////////

echo "Connecting to UWPR as webhosting...";

if ( ( $oracle = oci_connect( $dwhUser, $dwhPassword, $dwhDB ) ) === false ) {
    echo "ERROR: Unable to connect\n\n";
    exit(1);
}

echo "connected.\n\n";

// Generate Datasets and Send
///////////////////////////////

$count = count($datasets);

for ($x = 0; $x < $count; $x++) {
 
$query1 = "SELECT DISTINCT PERSONS.EMAIL, PERSONS.FNAME, PERSONS.LNAME 
FROM PERSONS, CONTACTS, ACCOUNTS, LISTS
WHERE PERSONS.PERSONID = CONTACTS.PERSONID
AND CONTACTS.CONTYPE != 3
AND CONTACTS.ACCOUNTID = ACCOUNTS.ACCOUNTID
AND ACCOUNTS.ACCSTAT = 4
AND ACCOUNTS.HOSTID = LISTS.ITEMID
AND LISTS.ALT_DATA = " . $datasets[$x]['string'] .
" ORDER BY PERSONS.EMAIL ASC";

$query2 = "SELECT DISTINCT PERSONS.EMAIL, PERSONS.FNAME, PERSONS.LNAME 
FROM PERSONS, CONTACTS, ACCOUNTS, LISTS
WHERE PERSONS.PERSONID = CONTACTS.PERSONID
AND CONTACTS.CONTYPE != 3
AND CONTACTS.ACCOUNTID = ACCOUNTS.ACCOUNTID
AND ACCOUNTS.ACCSTAT = 4
AND ACCOUNTS.ACCOUNTID IN " . $datasets[$x]['string'] .
" ORDER BY PERSONS.EMAIL ASC";

    $query_num = $datasets[$x]['query'];
    $query = ${"query$query_num"};

    $sth = oci_parse( $oracle, $query);

    echo "Writing " . $datasets[$x]['platform'] . " customers to " . $datasets[$x]['file'] . "...";

    oci_execute( $sth );
    
    $fp = fopen( "/usr/local/psa/bin/scripts/customerlists/" . $datasets[$x]['file'], "w" );
    
    $i = 0;
    while ( $row = oci_fetch_array( $sth ) ) {
        fwrite( $fp, ";;" . $row[1] . " " . $row[2] . ";" . $row[0] . "\n" );
        $i++;
    }
    
    fclose( $fp );
    
    $datasets[$x]['num'] = $i;
        
    echo "$i records found\n";
    
    oci_free_statement( $sth );
}



// Uploading Datasets
///////////////////////

echo "Uploading datasets...\n\n";

$cmd = "sftp -o \"IdentityFile /usr/local/psa/bin/scripts/customerlists/wisclist-custom-ssh\" -b /usr/local/psa/bin/scripts/customerlists/batchfile kdrop@drop.lists.wisc.edu 2>&1";
echo "Running command: $cmd\n\n";

$outputArray = array();
$returnValue = null;

exec( $cmd, $outputArray, $returnValue );

echo "Return Value: $returnValue\n\n";
foreach( $outputArray as $line ) echo $line . "\n";

oci_close( $oracle );


// Send Email
///////////////

$date = `date`;

$to = SENDTO;
$subject = "datasetGenerator.php Report";
$headers = "From: webhosting-notify@lists.wisc.edu\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\n";

$message = "<html><head><title></title></head><body style='background-color: black; color: lime; font-size: 10pt; font-family: 
monospace;'>";

$message .= "<h1>Shared Hosting Custom List Dataset Report</h1>";

$message .= "<p style='text-align: center;'><a href='https://custom.lists.wisc.edu' style='color: lime;'>Click here to access shared hosting customer lists</a></p>";

$message .= "<p><b>Date:</b> $date</p>";

$message .= "<table border=0 cellpadding=5 cellspacing=0 style='font-size: 10pt;'>";
    $message .= "<tr>";
        $message .= "<td style='border-bottom: 1px solid green;'>Platform</td>";
        $message .= "<td style='border-bottom: 1px solid green;'>Customers Found</td>";
    $message .= "</tr>";
    
    for ($x = 0; $x < $count; $x++)  {

        $message .= "<tr>";
            $message .= "<td>" . $datasets[$x]['platform'] . "</td>";
            $message .= "<td>" . number_format( $datasets[$x]['num'] ) . "</td>";
        $message .= "</tr>";
        
    }
    
$message .= "</table>";

$message .= "<p>Output from SFTP Command:</p>";
$message .= "<p>";
foreach( $outputArray as $line ) $message .= htmlspecialchars( $line ) . "<br>";
$message .= "</p>";

$message .= "</body></html>";

mail( $to, $subject, $message, $headers );

// Error Handler
//////////////////

function myErrorHandler( $errno, $errstr, $errfile, $errline, $errcontext ) {
    
    $currentdate = `date`;
    $currentdate = chop( $currentdate );
    
    $hostname = `hostname`;
    $hostname = chop( $hostname );
    
    $to = SENDTO;
    $subject = "Error in datasetGenerator.php";
    $headers = "From: webhosting-notify@lists.wisc.edu\n";
    $headers .= "X-Priority: 1\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    
    $message = "<html><head><title></title></head><body>";
    $message .= "<div style='text-align: center; margin: 10px;'>";
        $message .= "<img src='https://webhosting.doit.wisc.edu/images/billcosbyangry.jpg'>";
    $message .= "</div>";
    $message .= "<div style='margin: 10px; font-family: monospace; font-size: 12pt; color: black;'>";
        $message .= "Error Code: $errno<br><br>";
        $message .= "Error: $errstr<br><br>";
        $message .= "File: $errfile<br>";
        $message .= "Line Number: $errline<br>";
        $message .= "Time: $currentdate<br>";
        $message .= "Host: $hostname<br>";
    $message .= "</div>";
    
    $message .= "<p style='font-style: italic; font-family: serif; font-size; 10pt;'>A word to the wise ain't necessary - it's the stupid 
ones that need the advice. - Bill Cosby</p>";
    
    $message .= "</body></html>";
    
    mail( $to, $subject, $message, $headers );
    
    exit( 1 );
    
}

?>
