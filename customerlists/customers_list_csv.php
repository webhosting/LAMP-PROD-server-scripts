<?php

// All db connection variables are contained in the required db_conns.php.inc file
require __DIR__ . '/db_conns.php.inc';

define ( "SENDTO", "webhosting-notify@lists.wisc.edu");
define ( "USER", $dwhUser);
define ( "PASSWORD", $dwhPassword);
define ( "DB", $dwhDB);

error_reporting( E_ERROR | E_WARNING );
ini_set( "display_errors", true );

set_error_handler( "myErrorHandler", E_ERROR | E_WARNING );

$myFile = __DIR__ . '/shared_hosting_customers.csv';
$fh = fopen($myFile, 'w');

function get_customer_list($host,$id) {

	if ( ( $oracle = oci_connect( USER, PASSWORD, DB )  ) === false ) {
			
		$output = "Oracle connection error. Unable to ";
		exit(1);
		}

	else {
	$query = "SELECT distinct a.fullname, a.ACCOUNTID FROM properties p, accounts a ".
								"WHERE a.ACCSTAT=4 AND p.ACCOUNTID=a.ACCOUNTID ".
								"AND a.hostid = :hostid";
    
	$sth = oci_parse( $oracle, $query);
	oci_bind_by_name( $sth, ":hostid", $id );
	oci_execute( $sth );
	  
	
	while ($row = oci_fetch_array( $sth ))  {

		$names[] = $row['FULLNAME'];

		}

	oci_free_statement( $sth );
	
	if (count($names) === 0 ) {
		return;
	}
	
	else {
		natsort($names);
	
		foreach ($names as $name) {
	
			$query2 = "SELECT DISTINCT PROPERTIES.DESCRIPTION FROM PROPERTIES, ACCOUNTS 
			WHERE PROPERTIES.ACCOUNTID=ACCOUNTS.ACCOUNTID AND ACCOUNTS.FULLNAME = :name AND ACCOUNTS.ACCSTAT=4
			AND PROPERTIES.TITLE IN ('Bronze Service','Silver Service','Gold Service','Platinum Service','Secondary Domain')";
		    
			$sth = oci_parse( $oracle, $query2);
			oci_bind_by_name( $sth, ":name", $name );
			oci_execute( $sth );
		    
			  $output .= '"'.trim($name).'",';
	
				$temp_array = array();	
				while ($row = oci_fetch_array( $sth ))  {
				array_push($temp_array,trim($row['DESCRIPTION']));
	
				}
				$i = count($temp_array);			
				foreach ( $temp_array as $domain) {
				if ($domain == $temp_array[$i-1]) {
			      $output .=  '"'.trim($domain).'"';				
				}
				else
			      $output .=  '"'.trim($domain).'",';
			      }
			
		oci_free_statement( $sth );
	
		$output .=  "\n";			
		}
	}
	}

	return $output;
}

foreach ($hosts as $key => $value) {
	
	$output = get_customer_list($key,$value);
	$message .= $output;
	
}

fwrite($fh, $message);
fclose($fh);	

// Error Handler
//////////////////

function myErrorHandler( $errno, $errstr, $errfile, $errline, $errcontext ) {
    
    $currentdate = date("Y/m/d");
    $currentdate = chop( $currentdate );
    
    $hostname = `hostname`;
    $hostname = chop( $hostname );
    
    $to = SENDTO;
    $subject = "Error in customer_list_email.php";
    $headers = "From: webhosting-notify@lists.wisc.edu\n";
    $headers .= "X-Priority: 1\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    
    $message = "<html><head><title></title></head><body>";
    $message .= "<div style='text-align: center; margin: 10px;'>";
        $message .= "<img src='https://webhosting.doit.wisc.edu/images/billcosbyangry.jpg'>";
    $message .= "</div>";
    $message .= "<div style='margin: 10px; font-family: monospace; font-size: 12pt; color: black;'>";
       $message .= "Error Code: $errno<br><br>";
        $message .= "Error: $errstr<br><br>";
        $message .= "File: $errfile<br>";
        $message .= "Line Number: $errline<br>";
        $message .= "Time: $currentdate<br>";
        $message .= "Host: $hostname<br>";
    $message .= "</div>";
    
    $message .= "<p style='font-style: italic; font-family: serif; font-size; 10pt;'>A word to the wise ain't necessary - it's the stupid 
ones that need the advice. - Bill Cosby</p>";
    
    $message .= "</body></html>";
    
    mail( $to, $subject, $message, $headers );
    
    exit( 1 );
    
}

	?>


