# README #

This directory contains:

1. PHP scripts run as cron jobs to update Web Hosting’s account-specific WiscLists and custom WiscLists.
2. A PHP script run as a nightly cron job that emails a current list of Web Hosting's customers and sites.
3. A PHP script that can be run manually to generate a .csv file of Web Hosting's current customers and sites.

See full documentation of Web Hosting standard LAMP scripts at: https://wiki.doit.wisc.edu/confluence/display/DWH/LAMP+Scripts
