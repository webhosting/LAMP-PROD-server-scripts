<?php

// All db connection variables are contained in the required db_conns.php.inc file
require __DIR__ . '/db_conns.php.inc';

define ( "SENDTO", "webhosting-notify@lists.wisc.edu");

error_reporting( E_ERROR | E_WARNING );
ini_set( "display_errors", true );

set_error_handler( "myErrorHandler", E_ERROR | E_WARNING );

// Uploading Datasets
///////////////////////

echo "Uploading datasets...\n\n";

$cmd = "sftp -o \"IdentityFile /usr/local/psa/bin/scripts/customerlists/wisclist-custom-ssh\" -b /usr/local/psa/bin/scripts/customerlists/batchfile kdrop@drop.lists.wisc.edu 2>&1";
echo "Running command: $cmd\n\n";

$outputArray = array();
$returnValue = null;

exec( $cmd, $outputArray, $returnValue );

echo "Return Value: $returnValue\n\n";
foreach( $outputArray as $line ) echo $line . "\n";

// Send Email
///////////////

$date = `date`;

$to = SENDTO;
$subject = "datasetUploader.php Report";
$headers = "From: webhosting-notify@lists.wisc.edu\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\n";

$message = "<html><head><title></title></head><body style='background-color: black; color: lime; font-size: 10pt; font-family: 
monospace;'>";

$message .= "<h1>Shared Hosting Custom List Dataset Upload</h1>";

$message .= "<p style='text-align: center;'><a href='https://custom.lists.wisc.edu' style='color: lime;'>Click here to access shared hosting customer lists</a></p>";

$message .= "<p><b>Date:</b> $date</p>";

$message .= "<p>Output from SFTP Command:</p>";
$message .= "<p>";
foreach( $outputArray as $line ) $message .= htmlspecialchars( $line ) . "<br>";
$message .= "</p>";

$message .= "</body></html>";

mail( $to, $subject, $message, $headers );

// Error Handler
//////////////////

function myErrorHandler( $errno, $errstr, $errfile, $errline, $errcontext ) {
    
    $currentdate = `date`;
    $currentdate = chop( $currentdate );
    
    $hostname = `hostname`;
    $hostname = chop( $hostname );
    
    $to = SENDTO;
    $subject = "Error in datasetUploader.php";
    $headers = "From: webhosting-notify@lists.wisc.edu\n";
    $headers .= "X-Priority: 1\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    
    $message = "<html><head><title></title></head><body>";
    $message .= "<div style='text-align: center; margin: 10px;'>";
        $message .= "<img src='https://webhosting.doit.wisc.edu/images/billcosbyangry.jpg'>";
    $message .= "</div>";
    $message .= "<div style='margin: 10px; font-family: monospace; font-size: 12pt; color: black;'>";
        $message .= "Error Code: $errno<br><br>";
        $message .= "Error: $errstr<br><br>";
        $message .= "File: $errfile<br>";
        $message .= "Line Number: $errline<br>";
        $message .= "Time: $currentdate<br>";
        $message .= "Host: $hostname<br>";
    $message .= "</div>";
    
    $message .= "<p style='font-style: italic; font-family: serif; font-size; 10pt;'>A word to the wise ain't necessary - it's the stupid 
ones that need the advice. - Bill Cosby</p>";
    
    $message .= "</body></html>";
    
    mail( $to, $subject, $message, $headers );
    
    exit( 1 );
    
}

?>
