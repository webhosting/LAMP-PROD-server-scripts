#!/bin/bash
domain=$1
user=$2

# print error and exit if arguments aren't provided
if [[ !($domain) ]] || [[ !($user) ]]; 
then
echo -e "Correct format:\n./webhosting_keygen.sh <domain> <user>"
exit 1
fi

# check that .ssh directory exists in expected location
ssh=$(ls -a /var/www/vhosts/${domain} | grep '\.ssh')

# if it doesn't then create it
if [[ !($ssh) ]];
then 

mkdir /var/www/vhosts/${domain}/.ssh

fi

# check if authorized_keys already exists
keys=$(ls -a /var/www/vhosts/${domain}/.ssh | grep 'authorized\_keys')

# if it doesn't then create it
if [[ !($keys) ]];
then

touch /var/www/vhosts/${domain}/.ssh/authorized_keys

fi

# check if known_hosts already exists
hosts=$(ls -a /var/www/vhosts/${domain}/.ssh | grep 'known\_hosts')

# if it doesn't then create it
if [[ !($hosts) ]];
then

touch /var/www/vhosts/${domain}/.ssh/known_hosts

fi

# check	if id_rsa.pub already exists
pub=$(ls -a /var/www/vhosts/${domain}/.ssh | grep 'id\_rsa\.pub')

# if id_rsa.pub already exists, no ssh-keygen
# otherwise proceed
if [[ !($pub) ]];
then

# move to .ssh directory
cd /var/www/vhosts/${domain}/.ssh

# generate keypair adding note to identify the customer
ssh-keygen -t rsa -b 1024 -f id_rsa -C "Public SSH key for ${user} user" -N ''

# set ownership of directory and keypair to the correct user and group
chown -R ${user}:psacln /var/www/vhosts/${domain}/.ssh

fi
