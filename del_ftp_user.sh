#!/bin/bash

PRODUCT_ROOT_D=`grep ^PRODUCT_ROOT_D /etc/psa/psa.conf | awk '{print $2}'`
HTTPD_VHOSTS_D=`grep ^HTTPD_VHOSTS_D /etc/psa/psa.conf | awk '{print $2}'`
CHROOT_ROOT_D="$HTTPD_VHOSTS_D/chroot"

# Define an export a function to use with the find command for removing the
# user from the passwd file.  It expects both a user and a file passed to it.
function delete_user_from_passwd() {
    PRODUCT_ROOT_D=`grep ^PRODUCT_ROOT_D /etc/psa/psa.conf | awk '{print $2}'`
    HTTPD_VHOSTS_D=`grep ^HTTPD_VHOSTS_D /etc/psa/psa.conf | awk '{print $2}'`
    CHROOT_ROOT_D="$HTTPD_VHOSTS_D/chroot"

    if [ -z "${1}" ]; then
        echo "Missing username."
        exit 1
    fi
    if [ -z "${2}" ]; then
        echo "Missing passwd file."
        exit 1
    fi

    USER="${1}"
    PASSWD_FILE="${2}"

    # Remove the user from the passwd file.
    sed -i "/^${USER}:/d" ${PASSWD_FILE}
    # If the passwd file is empty, remove the entire chroot.
    if [ ! -s ${PASSWD_FILE} ]; then
        OLD_HOME_DIRECTORY=$(echo ${PASSWD_FILE} | sed -e 's/\/etc\/passwd//')
        ${PRODUCT_ROOT_D}/admin/sbin/chrootmng --remove --source=${CHROOT_ROOT_D} --target=${OLD_HOME_DIRECTORY}
        rm -rf ${OLD_HOME_DIRECTORY}/etc
    fi
}
export -f delete_user_from_passwd

# Remove the user from the chroot passwd file.  Despite the Plesk Documentation
# saying it exists, Plesk does not send the "OLD_HOME_DIRECTORY".  To get around
# this, find all the instances of passwd in their account and update them.
find "${HTTPD_VHOSTS_D}/${OLD_DOMAIN_NAME}" -name passwd -exec bash -c "delete_user_from_passwd ${OLD_SYSTEM_USER} \"{}\"" \;
