#!/bin/bash

OUTLOG=/usr/local/psa/bin/scripts/logs/delete_domain.log

echo "--------------"  >> $OUTLOG 2>> $OUTLOG

# information on the event date and time
/bin/date  >> $OUTLOG 2>> $OUTLOG

# information on the user, on behalf of which the script was executed (to ensure control)
/usr/bin/id >> $OUTLOG 2>> $OUTLOG

# information on the deleted physical hosting account
echo "Physical hosting deleted"  >> $OUTLOG 2>> $OUTLOG

# log domain name
echo "OLD_DOMAIN_NAME: ${OLD_DOMAIN_NAME}" >> $OUTLOG 2>> $OUTLOG

## remove domain to shibboleth2.xml
echo "/usr/local/psa/bin/scripts/shib2xml_updater.pl -mode remove -domain ${OLD_DOMAIN_NAME} running" >> $OUTLOG 2>> $OUTLOG

/usr/local/psa/bin/scripts/shib2xml_updater.pl -mode remove -domain ${OLD_DOMAIN_NAME} >> $OUTLOG 2>> $OUTLOG

echo "/usr/local/psa/bin/scripts/shib2xml_updater.pl -mode remove -domain ${OLD_DOMAIN_NAME} finished" >> $OUTLOG 2>> $OUTLOG

echo "--------------" >> $OUTLOG 2>> $OUTLOG
