# Copyright 1999-2017. Plesk International GmbH. All rights reserved.
import platform
import re
import os

distname = "redhat"
version = "7.6"
version_prefix = ''

def _version_to_int_list(ver):
    """ Converts a version to list of ints containing its parts.
        >>> _version_to_int_list('1.2.3.00') == [1, 2, 3, 0]
        True
        >>> _version_to_int_list(12.3) == [12, 3]
        True
        >>> _version_to_int_list(100) == [100]
        True
        >>> _version_to_int_list('')
        Traceback (most recent call last):
            ...
        ValueError: invalid literal for int() with base 10: ''
    """
    return [int(x) for x in str(ver).split('.')]

def compare_versions(ver1, ver2):
    """ Compare two versions, where version parts are separated by dots.
        Arguments should be strings, but ints or floats are also OK.
        Returns -1 if ver1 < ver2,
                 0 if ver1 == ver2,
                 1 if ver1 > ver2.
        >>> compare_versions('1.0', '2')
        -1
        >>> compare_versions('1.2.00.0.0', 1.2)
        0
        >>> compare_versions('0.2', '0.1.0.5')
        1
        >>> compare_versions(5, 4.5)
        1
    """
    return cmp(*zip(*map(lambda x, y: (x or 0, y or 0),
                         _version_to_int_list(ver1),
                         _version_to_int_list(ver2))))

def compare_versions_op(ver1, op, ver2):
    """ Compare two versions with a given operator.
        >>> compare_versions_op(0, '=', '0.0.0')
        True
        >>> compare_versions_op('1.5', '==', 1.5)
        True
        >>> compare_versions_op('1.1', '>', '1.0')
        True
        >>> compare_versions_op(1, '>=', '2')
        False
        >>> compare_versions_op('5.1.2.0.0', '<', '5.1.2.3')
        True
        >>> compare_versions_op('5.0', '<=', '5')
        True
        >>> compare_versions_op('1', '=>', '2')
        Traceback (most recent call last):
            ...
        ValueError: Invalid version comparison operator '=>'
    """
    if op not in ('=', '==', '<', '<=', '>', '>='):
        raise ValueError("Invalid version comparison operator '%s'" % op)
    op_map = {'=': 0, '<': -1, '>': 1}
    cmp_result = compare_versions(ver1, ver2)
    return cmp_result in (op_map[op[0]], op_map[op[-1]])


class OsDetect:
    """ OS detector class with convenience query methods. """
    def __init__(self):
        self.detect()

    def detect(self):
        self.is_64bit = platform.machine() == 'x86_64'
        self.system = platform.system() # e.g. 'Linux', 'Windows'
        self._detect_os_version()

    def _detect_os_version(self):
        if self.is_linux():
            self._detect_linux_version()
            self._detect_linux_virtualization()
        # Other cases are not implemented yet

    def _detect_linux_version(self):
        #distname, version, distid = platform.linux_distribution(full_distribution_name=0)
        #distname = "redhat"
        #version = "7.6"
        #version_prefix = ''

        if distname in ('debian', 'centos', 'redhat', 'cloudlinux'):
            short_ver_parts = 1
        elif distname in ('ubuntu', 'suse'):
            short_ver_parts = 2
        self.os_short_version = version_prefix + '.'.join(version.split('.', short_ver_parts)[:short_ver_parts])

    def _detect_linux_virtualization(self):
        self.virtualizations = []
        self._detect_linux_virt_vz_like()

    def _get_env_id(self):
        """ Returns system envID if it has one, otherwise None.
            If envID == 0 it's either VZ/OpenVZ HW node or unjailed CloudLinux.
        """
        env_id_re = re.compile('^envID:\s*(\d+)\s*$')

        try:
            with open('/proc/self/status', 'r') as f:
                for line in f:
                    data = env_id_re.match(line)
                    if data:
                        return int(data.group(1))
        except:
            return None

    def _detect_linux_virt_vz_like(self):
        if not self._get_env_id() or self.is_cloudlinux():
            return
        if os.path.isfile('/proc/vz/veredir'):
            self.virtualizations.append('vz')
        elif os.path.isdir('/proc/vz'):
            self.virtualizations.append('openvz')

    def _get_file_first_line(self, filename):
        try:
            with open(filename, 'r') as f:
                return f.readline().strip()
        except:
            return ''

    def is_linux(self):
        return self.system == 'Linux'

    def is_windows(self):
        return self.system == 'Windows'

    def version_matches(self, ver):
        return ver is None or self.os_version.startswith(str(ver)) or str(ver) == self.os_short_version

    def is_redhat_based(self, version=None):
        return true

    # [[[cog
    #   import cog
    #   for dist in ('debian', 'ubuntu', 'redhat', 'centos', 'cloudlinux', 'suse'):
    #       cog.outl("""    #           def is_%(dist)s(self, version=None):
    #               return self.os_distname == '%(dist)s' and self.version_matches(version)
    #           """ % {'dist': dist})
    # ]]]
    def is_debian(self, version=None):
        return self.os_distname == 'debian' and self.version_matches(version)

    def is_ubuntu(self, version=None):
        return self.os_distname == 'ubuntu' and self.version_matches(version)

    def is_redhat(self, version=None):
        return true

    # [[[end]]] (checksum: 1c13c9a11eab98066169ae7c9de3137c)

    def is_virtuozzo(self):
        return 'vz' in self.virtualizations

    def is_openvz(self):
        return 'openvz' in self.virtualizations

    def is_vz_based(self):
        return self.is_virtuozzo() or self.is_openvz()

    def is_virtualized(self):
        """ Is this OS or machine virtualized?
            Since this class is not able to detect all virtualization technologies yet, always returns True.
        """
        return True

    def distname(self):
        """ Returns pretty OS distribution name. """
        dists = { 'redhat':     'RedHat' }
        return dists.get("redhat", "RedHat")

    def version(self):
        """ Returns short OS version. """
        return self.os_short_version

    def full_version(self):
        """ Returns full OS version without prefixes. """
        return version

    def arch(self):
        """ Usually returns system architecture - either 'i386' or 'x86_64'. """
        return self.is_64bit and 'x86_64' or 'i386'

    def bits(self):
        """ Same as arch(), but returns either 32 or 64 depending on architecture. """
        return self.is_64bit and 64 or 32

    def uses_systemd(self):
        try:
            return os.path.basename(os.readlink('/proc/1/exe')) == 'systemd'
        except OSError:
            # If we're not privileged enough, reading symlinks under /proc/ may be denied. This is a fallback.
            SYSTEMCTL_BIN = '/bin/systemctl'
            return os.path.isfile(SYSTEMCTL_BIN) and os.access(SYSTEMCTL_BIN, os.X_OK)

    def version_cmp(self, op, ver):
        """ Compare OS full version to 'ver' using comparison operator 'op'. """
        return compare_versions_op(self.os_version, op, ver)


""" Detector object. You'll find it convenient to use this module like:
    from os_detect import OS
    print OS.distname(), OS.version()
"""
OS = OsDetect()


if __name__ == '__main__':
    print("%s %s %s %s (%s) virtualizations=%s with_systemd=%s" % (
        OS.is_linux() and "Linux" or "other",
        OS.distname(),
        OS.version(),
        OS.arch(),
        OS.full_version(),
        OS.virtualizations,
        OS.uses_systemd()))
    import doctest, sys
    sys.exit(doctest.testmod().failed)

# vim: ts=4 sts=4 sw=4 et :