# README #

For each host, you must edit the **host_values** file. The purpose of this file is to 1) add local MySQL db user with admin rights to all DBs 2) password for that user and 3) friendly name of local host server, in order for the scripts that query MySQL to work correctly.

See full documentation of Web Hosting standard LAMP scripts at: https://wiki.doit.wisc.edu/confluence/display/DWH/LAMP+Scripts

The add/del/mod_ftp_user.sh scripts modify the behavior of the Plesk Additional FTP Account function.  If they are in place, instead of creating an FTP user, the scripts will create new chrooted accounts with access to the domain.  The home directory specified will be the root directory of the new user.

# REQUIREMENTS #
perl-XML-Writer
perl-XML-LibXML
