#!/bin/bash

OUTLOG=/usr/local/psa/bin/scripts/logs/create_domain_${NEW_DOMAIN_NAME}.log

echo "--------------" > $OUTLOG 2>$OUTLOG
echo "--------------physical_hosting" >> $OUTLOG 2>> $OUTLOG

# information on the event date and time
/bin/date >> $OUTLOG 2>>$OUTLOG

# information on the user, on behalf of which the script was executed (to ensure control)
/usr/bin/id >> $OUTLOG 2>> $OUTLOG

# information on the created physical hosting account
echo "Physical hosting created" >> $OUTLOG 2>> $OUTLOG

# log domain name
echo "NEW_DOMAIN_NAME: ${NEW_DOMAIN_NAME}" >> $OUTLOG 2>> $OUTLOG

# log account username
echo "NEW_SYSTEM_USER: ${NEW_SYSTEM_USER}" >> $OUTLOG 2>> $OUTLOG
echo "NEW_FP_ADMIN_PASSWORD: ${NEW_FP_ADMIN_PASSWORD}" >> $OUTLOG 2>> $OUTLOG

#Add private/php_sess and remove picture_library
echo "Making changes to default webspace directories" >> $OUTLOG 2>> $OUTLOG

mkdir /var/www/vhosts/${NEW_DOMAIN_NAME}/private
chown ${NEW_SYSTEM_USER}:psacln /var/www/vhosts/${NEW_DOMAIN_NAME}/private
chmod 711 /var/www/vhosts/${NEW_DOMAIN_NAME}/private/
mkdir /var/www/vhosts/${NEW_DOMAIN_NAME}/private/php_sess
chown ${NEW_SYSTEM_USER}:psacln /var/www/vhosts/${NEW_DOMAIN_NAME}/private/php_sess
chmod 700 /var/www/vhosts/${NEW_DOMAIN_NAME}/private/php_sess
rm -r /var/www/vhosts/${NEW_DOMAIN_NAME}/httpdocs/picture_library

echo "Added private/php_sess directory and removed picture_library" >> $OUTLOG 2>> $OUTLOG

## check if log config file exists
echo "Making sure logging config file was created for domain" >> $OUTLOG 2>> $OUTLOG
FILE=/usr/local/psa/etc/logrotate.d/${NEW_DOMAIN_NAME}

if [ -f $FILE ];
then
   echo "Log rotator config file OK ($FILE)" >> $OUTLOG 2>> $OUTLOG

else
   echo "**WARNING: Log rotator config file ($FILE) does not exist.
           Enabling log rotation via command line.**" >> $OUTLOG 2>> $OUTLOG

   echo "Log rotator config file ($FILE) did not exist for newly created domain.
           Enabled log rotation via command line." | mail -s "**Log rotation setup warning for ${NEW_DOMAIN_NAME}**" webhosting-notify@lists.wisc.edu

/usr/local/psa/bin/site -u ${NEW_DOMAIN_NAME} -log_rotate true

fi

## add dateext directive to each section of log rotator file for new domain
#  so that log files have date appended to their name
sed 's/\}/\tdateext\n\}/g' /usr/local/psa/etc/logrotate.d/${NEW_DOMAIN_NAME}  > /tmp/${NEW_DOMAIN_NAME}_logrotate_tmpfile
mv /tmp/${NEW_DOMAIN_NAME}_logrotate_tmpfile /usr/local/psa/etc/logrotate.d/${NEW_DOMAIN_NAME}

## clear qmail
cp -p /var/qmail/control/virtualdomains_empty /var/qmail/control/virtualdomains
/etc/init.d/qmail restart
echo "Cleared Qmail virtualdomains file"  >> $OUTLOG 2>> $OUTLOG


## add domain to shibboleth2.xml
echo "Adding SP for new domain to shibboleth2.xml" >> $OUTLOG 2>> $OUTLOG

echo "/usr/local/psa/bin/scripts/shib2xml_updater.pl -mode add -domain ${NEW_DOMAIN_NAME} running" >> $OUTLOG 2>> $OUTLOG

/usr/local/psa/bin/scripts/shib2xml_updater.pl -mode add -domain ${NEW_DOMAIN_NAME} >> $OUTLOG 2>> $OUTLOG

echo "/usr/local/psa/bin/scripts/shib2xml_updater.pl -mode add -domain ${NEW_DOMAIN_NAME} finished" >> $OUTLOG 2>> $OUTLOG


## generate SSH keypair for customer
echo "Adding SSH keypair for customer's system user" >> $OUTLOG 2>> $OUTLOG

/usr/local/psa/bin/scripts/webhosting_keygen.sh ${NEW_DOMAIN_NAME} ${NEW_SYSTEM_USER} >> $OUTLOG 2>> $OUTLOG

echo "/usr/local/psa/bin/scripts/webhosting_keygen.sh ${NEW_DOMAIN_NAME} ${NEW_SYSTEM_USER} finished" >> $OUTLOG 2>> $OUTLOG

echo "--------------" >> $OUTLOG 2>> $OUTLOG
